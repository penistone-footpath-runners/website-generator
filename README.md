# PFR Website Generator

This is a tool for generating the websites for Penistone Footpath Runners' club
competitions. Run `./update.py -h` for help, or see the documentation at
https://penistone-footpath-runners.gitlab.io/website-generator/.
