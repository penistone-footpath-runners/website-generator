Club Records
============

Procedure
---------

#. Ensure the members are up to date.
#. Replace the ``All club records.csv`` file.
#. Run the update script.
#. Upload ``records/`` to website, along with the runner pages.

Data File
---------

The file contains the main content for the website. The basic structure is a CSV
file, but separated by tildes (``~``) instead of commas (commas are useful to
have in the field names). The first row contains the headings.

In addition, before each row is a number, followed by any character.  This makes
maintaining a spreadsheet easier, and is removed by the script. As an example,
the current heading row is:

::

   0,Type~Sex~Age~Event~Note~Performance~Athlete~Location~Date

Sorting File
------------

The order in which the categories appear in the filter table is controlled by
``filters.txt``. The format is:

-  Each row is split into two parts, delimited by an ``=``.
-  Before the ``=`` is the name of the heading.
-  After the ``=``, there is a comma-separated list of the heading items, in the
   order in which they will appear in the table.

Club Records Detection
----------------------

Some new club records can be detected semi-automatically from the UKA website.
Here is the procedure for getting them:

#. In the Road Standards:

   #. Set ``GET_RECORDS`` to ``True`` in ``main.py``.
   #. Run ``main.py``. (You may want the ``-d`` argument, to re-download all of
      the data.)
   #. Copy the dictionary that is printed (between the progress bar and the
      error).

#. In the Club Records:

   #. Paste the dictionary into a file.
   #. Run
      .. code-block:: console

         $ ./checker.py INPUT_PATH NEW_FILE

      where INPUT_PATH is the normal input directory and NEW_FILE is the one
      just created.
   #. A list of possible new records will be printed. At the end is a list of
      events that are not counted.

This should get all records on the UKA website with a few limitations:

-  Junior records are missed (since the Road Standards ignores juniors).
-  V35 records are missed.
-  It counts every race as both track and road, which causes false positives.

It is probably not necessary to run this very often since the first time I used
this function it only found four new records and an error in the Club Records
input data.
