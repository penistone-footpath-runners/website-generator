Road Standards
==============

This scrapes data from the UKA website. The UKA website is significantly less
hostile than Parkrun, and it can be scraped much faster. However, it could still
be broken at any time by a website reformat.

Procedure
---------

#. Ensure the members are up to date.
#. Run the update script.
#. Upload ``RoadStandards/`` to the website, along with the runner pages.

Additional and Removed Races
----------------------------

The files ``Additional races for club standards.csv`` and ``Illegal races for
club standards.csv`` give extra races which are either to be added or removed.

The format for both is a CSV with the following fields:

-  ``Name`` (of runner)
-  ``Event`` (race distance)
-  ``Time``, in [HH:]MM:SS
-  ``Race``
-  ``Date``, in UK format


Saved Pages
-----------

The Road Standards script saves the downloaded tables to ``savedpages.json.xz``.
There is no reason to touch this file.

Standards Calculation Process
-----------------------------

The standards are calculated from Alan Jones’ WMA age-standards tables, which
are available at https://www.runscore.com/Alan/AgeGrade.html. The
``factors.csv`` file contains the main tables of the ‘AgeStdFactors’ sheets of
both 2015 tables side by side, including row and column headings. The
file ``standards2.py``, which was named this way to allow it to be checked
against the older ``standards.py`` (as well as to avoid name collisions), does
the calculations to transform these tables into minimum times to achieve a given
standard.

The standards revolve around the idea of an *age grade*. This is a runner’s
average speed for a race as a proportion of the world record average speed for
that distance, by someone of the same age and sex as that runner. The world
record average speeds are actually a model, because some unusual distances have
‘softer’ records. Age grades are usually expressed as percentages. The
calculations for the standards are designed such that in order to qualify for a
particular standard, you *must* have obtained a certain age-graded percentage.
These minimum percentages are:

================= =================
Standard Achieved Minimum Age Grade
================= =================
1st Class         75 %
2nd Class         65 %
3rd Class         55 %
4th Class         45 %
================= =================

Within the column for the appropriate distance, the time in the ‘OC sec’ row
gives a modelled world-record time, for a male or female of optimal age. For
each age, this time can be divided by the corresponding entry in the main body
of the table to get a modelled world-record time for that age. This gives a time
for 100 %. Dividing this by the percentage values for each standard gives the
exact time required to achieve that standard.

These exact values are then rounded down. Times less than half an hour are
rounded down to the nearest 15 s. Times greater than an hour are rounded down to
the nearest minute. Other times are rounded down to the nearest 30 s
