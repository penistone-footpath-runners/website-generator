Welcome to PFR Website Generator's documentation!
=================================================

.. toctree::
   :maxdepth: 2

   club-records
   parkrun
   championship-fell-league
   club-standards
   runner-pages
   winter-handicap


General Notes
=============

For this documentation, when a specific year is needed it is taken to be
2020. The procedure for updating the competitions to a new year is described in
:ref:`new-year`.

Prerequisites
=============

The website is updated by pushing to a Git repository, so Git is needed.

Python
------

The scripts require Python 3.9 or later, and the ``pandas``, ``tqdm`` and
``requests`` packages for some competitions.

Obtaining the Scripts
=====================

The source code for the competitions can be cloned from the Git repository at
https://gitlab.com/penistone-footpath-runners/website-generator. You may use the
following command to fetch all of the competition data:

.. code-block:: console

   $ git clone https://gitlab.com/penistone-footpath-runners/website-generator.git

Before running each script, some data files are required. The formats for these
data files are described in this documentation.

The Competitions Script
=======================

To update a competition, run the following command:

.. code-block:: console

   $ ./update.py INPUT_PATH COMPETITION

``COMPETITION`` must be one of

-  ``championship``
-  ``fell_league``
-  ``records``
-  ``standards``
-  ``parkrun``
-  ``senior_handicap``
-  ``runner_pages``

``INPUT_PATH`` is the path to the data files.

The ``-y`` flag can be used to run a competition for a year other than the
current one which defaults to the current year if missing.

The ``-r`` specifies that the runner pages should also be re-build afterwards.
The ``-d`` flag, which applies only to the Road Standards and Parkrun
Competition, indicates that new results should be downloaded from the website.

``members.csv``
===============

This file gives information about club members for all competitions (except the
Junior Handicap).

The basic columns are:

.. list-table::
   :widths: auto

   * - ``System name``
     - The runner's name. Every competition (except the Junior Handicap) uses
       this. It is required to be unique.
   * - ``Date of birth``
     - The member's date of birth, in ISO format. This is used by the Club
       Championship, Standards and Senior Handicap.
   * - ``Sex``
     - Either ``Male`` or ``Female``. Used by the Road Standards, Club Records,
       Club Championship and Senior Handicap.
   * - ``Member since``
     - The date the runner first joined. It is used by the Road Standards.
   * - ``First claim fell``
     - Eligibility for the Fell League.
   * - ``First claim other``
     - Eligibility for the Club Championship.
   * - ``Power of 10 ID``
     - ID for the Power of 10 website.
   * - ``Parkrun ID``
     - ID for the Parkrun website.

.. _new-year:

Updating to a New Year
======================

The Club Records and Road Standards do not need updating, but a snapshot of the
Standards may be desirable. This can be taken from ``index-2020``, an HTML file
which is hidden by default, but contains all of the Standards achieved in a
concise format.

I would recommend making a copy of your ``Competitions/`` directory for the new
year, so that mistakes in last year’s competitions can be easily fixed, and the
configuration files are not lost. Then delete the directories containing the
website HTML before updating the competitions.

Parkrun Competition
-------------------

Copy ``not_in_club_2019.txt`` to ``not_in_club_2020.txt``.

Club Championship and Fell League
---------------------------------

Add a ``RaceResults/2020`` directory.


Winter Handicap
---------------

Add a ``SeniorWinterHandicap/2020`` directory, as a copy of the previous year.
``dates.txt`` must be updated to contain the new race dates. The format for this
file is described in :ref:`handicap-dates`. It consists of a newline-separated
list of dates in ISO format.

Runner Pages
------------

Update ``runner_page_years.txt``.

.. _index-page-1:

Index Page
----------

The ``index.html`` file should be fairly legible HTML. Add a new year section in
the table, with links to the new website URLs, which should take the same format
as the old ones.
