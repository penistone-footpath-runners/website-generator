Parkrun
=======

These results are scraped from the Parkrun website. They have explicitly tried
to prevent this, which the script circumvents by using a false User-Agent. This
is currently a fake browser string. Still, the Parkrun website starts blocking
an IP address if it sends too many requests, so the queries are separated by
seven seconds to prevent detection. This mostly works, but still seem to block
it occasionally, and the only way to fix this seems to be to wait.  If your IP
address is blocked frequently, increasing the delay between requests will
probably help, but this will make the program take longer to run.

If they reformat their website, this script will probably stop working. This has
happened a few times. Generally it is easy to fix. Parkrun could make it
impossible to run (for example, by no longer showing a column of data that the
program needs access to).

Procedure
---------

#. Ensure the members are up to date.
#. Run the update script.
#. Upload ``parkrun2020/`` to the website, along with the runner pages.

.. _reset-parkrun:

Redownloading
-------------

It is useful occasionally to redownload all Parkrun results for the year,
because occasionally results will be uploaded late, and the program will miss
them (because they did not exist when it was run). To do this, run the Parkrun
with the ``--redownload-all`` argument if you want to re-download everything. It
will take about three times longer to run if you do this (the difference in time
increases throughout the year). I recommend re-downloading a few times
throughout the year, especially near the end.

Most Popular Parkruns
---------------------

The script writes some data about Parkrun popularity to ``number_data.csv``. It
doesn’t make any use of this itself, it is merely for curiosity.
