Championship and Fell League
============================

The Championship and Fell League are somewhat integrated, due to some of the
same races being included in both.

Procedure
---------

#. Ensure the members are up to date.
#. Add the necessary race files to the ``RaceResults/2020/`` directory.
#. Run the update script.
#. Upload ``Championship2020/`` or ``FellLeague2020/`` to the website, as well
   as the runner pages.

Race Files
----------

These files, stored in ``RaceResults/2020/*.csv``, contain the data for each
race.

Header
~~~~~~

This section may contain trailing commas to make the file ‘rectangular’, but
they are not necessary.

-  The first line contains Championship eligibility, in the format
   ``Championship,x``, where ``x`` is either ``FALSE``, or ``TRUE``.
-  The second line similarly gives Fell League eligibility in ``Fell League,x``
   form.
-  The third line contains the race name. The race name must be the same as the
   filename, ignoring the file extension.
-  The fourth line gives the date (in DD/MM/YYYY format).
-  The fifth line gives the category (either ``Fell``, ``Cross Country`` or
   ``Road``).

Body
~~~~

The rest of the file contains race result data. It is in a format of
``name,x,time``. x is gender for the winner rows, and position in the other
rows.

The first two rows contain the male and female winners, respectively. Their
names are just treated as text. Currently their clubs are placed, parenthesised,
after their names. This is not required by the program.  Their times necessary
for the points calculation.

Following the winners’ times is a single blank line. The rest of the file
contains the club runners in finishing order.

Points Calculations
-------------------

The points for the Championship and Fell League are calculated in ``points.py``
by the ``points()`` function. The current method was designed such that a
constant multiplicative decrease in pace results in a constant linear decrease
in points. The points, :math:`P`, are normally calculated using this equation:

.. math:: P = 100 + \frac{50}{\log{2}}\cdot\log \frac{w}{r}
          :label: points

where :math:`w` is the winner’s time, and :math:`r` is the individual runner’s
time (both converted to a number of seconds). The bases of the logarithms do not
matter, as long as they are equal. A slightly different form of this equation is
present on the website and in the code.

If the race is some form of score event, a different formula from :eq:`points`
is used. This is for two reasons:

-  In score events, more points are better. This would mean that naı̈vely using
   the same formula would result in the scores being the wrong way round. The
   winner’s ‘time’ would be higher than the other runner’s ‘times’.
-  The differences in points are typically much larger on score events than in
   timed events, and this results in very low scores. The score would become
   negative if the winner scores four times as many points as them. A ratio of
   10:1 would not be inconceivable, and would result in approximately
   :math:`-66` points, which could be rather discouraging to runners. This is
   the reason that :eq:`points` is not simply used ‘in reverse’, with :math:`r`
   and :math:`w` swapped.

The formula actually used for score events is this:

.. math:: P = 100 \cdot r / w

where, again, :math:`P` is the number of points awarded, :math:`w` is the
winner’s score and :math:`r` is the individual runner’s score.

Note that negative points are never awarded: if :math:`P` is negative, it is
made zero.
