Runner Pages
============

Procedure
---------

#. Ensure the other competitions have already been updated.
#. Run the update script.
#. Upload ``Runner Pages/`` to the website.

There are no separate configuration files for the Runner Pages. The HTML
snippets from the other competitions are automatically stored in, when those
competitions are run, and the Runner Pages script assembles them into a single
page.
