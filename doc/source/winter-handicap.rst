Winter Handicap
===============

Senior Handicap
---------------

Procedure
~~~~~~~~~

#. Ensure the members and ``SeniorRunners.csv`` are up to date.
#. Add the race files to the results directory.
#. Run the update script.
#. Upload ``SeniorWinterHandicap2020/`` to the website, along with the runner
   pages.

Runners File
~~~~~~~~~~~~

``SeniorRunners.csv`` is formatted as a CSV file, with a row per runner.  The
fields are as follows.

-  The first column gives the name of the runner.
-  The second column has the runner’s handicap. This is formatted as HH:MM:SS,
   with a preceding ``-`` for negative handicaps.

.. _handicap-dates:

Dates File
~~~~~~~~~~

The dates of all of the races for the season are stored in ``dates.txt``. This
file is a newline-separated list, containing one date (in ISO format) per line.

This is created at the start of the season, and does not need to be changed
until the start of next season, or if a race is rescheduled.

Start Times
~~~~~~~~~~~

The script calculates predicted times for the next race, and gives some
additional information which might help in adjusting them. This is saved to
``senior-winter-handicap-2020/start-times``.

Junior Handicap
---------------

Procedure
~~~~~~~~~

#. Add the race files to the ``results/`` directory.
#. Update the ``runners.csv`` file.
#. Run ``main.py``.
#. Any errors are either bugs or due to mistakes in the members or race
   files.
#. If any errors were fixed, re-run ``main.py``.
#. Upload ``junior-winter-handicap-2020/`` to the website.

Runners File
~~~~~~~~~~~~

``runners.csv`` is also formatted as a CSV file for the Junior Handicap, with
one row per runner. A PB can be real or artificial. An artificial PB will be
overwritten by any time, and is used for new runners or runners changing age
group. The fields are, in order:

-  The first column gives the name of the runner.
-  The second column gives the runner’s sex (``F`` or ``M``).
-  The third column contains the runner’s age, as an integer.
-  The fourth column gives their category. This is in the format ``Under x``,
   where ``x`` is either ``11``, ``13``, ``15`` or ``17``.
-  The fifth column contains the runner’s handicap, formatted as HH:MM:SS.
-  The final column is a single bit, as a ``0`` or ``1`` character, toggling
   whether the PB is a real PB or an artificial one.
