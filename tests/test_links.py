import re
from pathlib import Path

YEAR = 2022

NON_YEARS = [str(y) for y in range(YEAR + 10) if y != YEAR]


def should_ignore(link):
    return any(b in link for b in (*NON_YEARS, "://", ".svg", ".git"))


def test_build_links() -> None:
    for file in Path("build").rglob("*"):
        if file.is_dir() or should_ignore(str(file)):
            continue
        for link in re.findall(r'href="(.+?)"', file.read_text()):
            if should_ignore(link):
                continue
            assert (file.parent / link).exists() or (
                file.parent / f"{link}.html"
            ).exists()
