#!/usr/bin/env python3
"""Update the competitions."""

import argparse
import shutil
from datetime import date
from pathlib import Path


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("input", type=Path, help="input files location")
    parser.add_argument(
        "competition",
        type=str,
        choices=(
            "championship",
            "fell_league",
            "records",
            "awards",
            "standards",
            "parkrun",
            "senior_handicap",
            "junior_handicap",
            "runner_pages",
            "new_records",
        ),
        help="competition to update",
    )
    parser.add_argument("--output", type=Path, help="output directory")
    parser.add_argument(
        "-r", "--runner-pages", action="store_true", help="also update runner pages"
    )
    parser.add_argument(
        "-d",
        "--download",
        action="store_true",
        help="download new data; only relevant for standards and Parkrun",
    )
    parser.add_argument(
        "--redownload-all",
        action="store_true",
        help="re-download all past Parkrun results (implies -d)",
    )
    parser.add_argument(
        "-y", "--year", type=int, help="year of competition to run", default=None
    )
    args = parser.parse_args()

    if args.year is None:
        today = date.today()
        year = today.year
        competition_year = year
        if today.month < 4:
            competition_year -= 1
    else:
        year = args.year
        competition_year = args.year

    local_path = Path(__file__).resolve().parent
    output = args.output if args.output else local_path / "build"
    output.mkdir(exist_ok=True)
    for filename in ("main.css", "print.css", "logo.svg", "index.html"):
        shutil.copy2(local_path / "competitions" / filename, output / filename)

    match args.competition:
        case "championship":
            from competitions.club_championship import championship

            championship.update_championship(args.input, output, competition_year)
        case "fell_league":
            from competitions.club_championship import fellleague

            fellleague.update_fell_league(args.input, output, competition_year)
        case "records":
            from competitions.club_records import main

            main.update_records(args.input, output)
        case "awards":
            from competitions.awards import main

            main.update_awards(args.input, output)
        case "senior_handicap":
            from competitions.senior_winter_handicap import main

            main.update_handicap(args.input, output, year)
        case "junior_handicap":
            from competitions.junior_winter_handicap import main

            main.make_pages(args.input, output, year)
        case "standards":
            from competitions.road_standards import main

            main.make_pages(args.input, output, year, dl=args.download)
        case "parkrun":
            from competitions.parkrun import generate_html

            if args.download or args.redownload_all:
                from competitions.parkrun import download

                download.main(args.input, year, args.redownload_all)
            generate_html.main(args.input, output, year)
        case "new_records":
            from competitions.club_records import checker
            from competitions.road_standards import main

            records = main.make_pages(
                args.input, output, year, dl=args.download, get_records=True
            )
            checker.main(args.input, records)

    if args.runner_pages or args.competition == "runner_pages":
        from competitions.runner_pages import main

        main.update_runner_pages(args.input, output)


if __name__ == "__main__":
    main()
