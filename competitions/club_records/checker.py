#!/usr/bin/env python3
import itertools
from pathlib import Path

from competitions.constants import SEXES
from competitions.filterable_table import load_data
from competitions.road_standards.main import time_to_secs

LOCAL_PATH = Path(__file__).resolve().parent
AGE_MAP = {
    "U11": None,
    "U13": None,
    "U15": None,
    "U17": None,
    "U20": None,
    "Sen": "Senior",
    "V35": None,
    "V40": "40–44",
    "V45": "45–49",
    "V50": "50–54",
    "V55": "55–59",
    "V60": "60–64",
    "V65": "65–69",
    "V70": "70–74",
    "V75": "75–79",
    "V80": "80+",
}

EVENT_MAP = {
    "High jump": "HJ",
    "Long jump": "LJ",
    "Pole vault": "PV",
    "Triple jump": "TJ",
    "Marathon": "Mar",
    "½ marathon": "HM",
    "⅓ marathon": "8.74M",
    "Mile (track)": "Mile",
    "Mile (road)": "Mile",
    "2M (track)": "2Miles",
    "Coniston 14": "13.83M",
}

THROW_MAP = {
    "M": {
        "Javelin": "JT800",
        "Discus": "DT2K",
        "Hammer": "HT7.26K",
        "Shot": "SP7.26K",
        "Cricket ball": None,
        "Rounders ball": None,
    },
    "F": {
        "Javelin": "JT600",
        "Discus": "DT1K",
        "Hammer": "HT4K",
        "Shot": "SP4K",
        "Cricket ball": None,
        "Rounders ball": None,
    },
}


def event_map(event, gender):
    if event[-1] == "M":
        return event
    if event[-2:] == "km":
        return event.replace("km", "K")
    if event[-1] == "m":
        return event.replace("m", "")
    if event[-3:] == "m H":
        return event.replace("m H", "H")
    if event[-4:] == "m SC":
        return event.replace("m ", "")
    if event in EVENT_MAP:
        return EVENT_MAP[event]
    if event in THROW_MAP[gender]:
        return THROW_MAP[gender][event]
    raise ValueError(f"unknown event: {event}")


def main(input_path, dled):
    _, datalines = load_data(input_path / "ClubRecords" / "All club records.csv")

    # Get sorting data
    with open(LOCAL_PATH / "filters.txt") as sd:
        sd_text = sd.read().splitlines()
    events = sd_text[3][6:].split(",")

    combs = itertools.product(SEXES, AGE_MAP.keys(), events)
    bad_events = set()
    for g, c, e in combs:
        category = AGE_MAP[c]
        event = event_map(e, g)
        if event is None:
            bad_events.add(e)
            continue
        if category is None:
            continue
        try:
            bestnew = dled[(g, category)][event]
        except KeyError:
            continue
        for row in datalines:
            if row[1] == g and row[2] == c and row[3] == e:
                break
        else:
            print("Old: None")
            print("New: " + str(bestnew))
            print()
            continue
        if "m" in row[5]:
            if float(bestnew[3].replace("m", "")) > float(row[5].replace("m", "")):
                print("Old: " + str(row))
                print("New: " + str(bestnew))
                print()
            continue
        if time_to_secs(bestnew[3]) < time_to_secs(row[5].replace("s", "")):
            print("Old: " + str(row))
            print("New: " + str(bestnew))
            print()
    print("Bad Events: " + ", ".join(bad_events))
