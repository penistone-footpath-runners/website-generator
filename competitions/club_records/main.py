from pathlib import Path

from competitions import filterable_table, make_page, read_members, runner_pages

LOCAL_PATH = Path(__file__).resolve().parent


def update_records(input_path, html_path):
    html_path /= "records"
    writer = make_page.Writer("PFR Club Records", "Current Club Records", html_path)

    filters = tuple(filterable_table.get_filters(LOCAL_PATH / "filters.txt"))
    fields, entries = filterable_table.load_data(
        input_path / "ClubRecords" / "All club records.csv", ignore_number_col=True
    )

    writer.write("index", filterable_table.create_page(fields, entries, filters))

    members = read_members.MembersList(input_path / "members.csv").runner_names

    runner_fragments = {
        runner: filterable_table.create_fragment(fields, results)
        for runner in members
        if (results := [x for x in entries if runner in x[6]])
    }

    runner_pages.provide(input_path, "Club Records", runner_fragments)
