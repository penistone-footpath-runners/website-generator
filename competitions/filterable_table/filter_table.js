function filter(filter) {
  for (const other_filter of filter.parentElement.children) {
    other_filter.classList.remove("highlight");
  }
  filter.classList.add("highlight");
  refilter_table();
}
function refilter_table() {
  let allowed = {};
  for (const filter of document.getElementById("filters").children) {
    const value = Array.from(filter.children).find((option) =>
      option.classList.contains("highlight"),
    ).textContent;
    if (value !== "All") {
      allowed[filter.children[0].textContent] = value;
    }
  }
  const headings = Array.from(document.getElementById("headings").children).map(
    (h) => h.textContent,
  );
  for (const row of document.getElementById("filterable").children) {
    row.hidden = headings.some(
      (heading, i) =>
        Object.hasOwn(allowed, heading)
        && row.children[i].textContent !== allowed[heading],
    );
  }
}
