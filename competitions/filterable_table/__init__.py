import string
from collections.abc import Iterable, Iterator
from dataclasses import dataclass
from pathlib import Path

from competitions.html_gen import tag

LOCAL_PATH = Path(__file__).resolve().parent


@dataclass
class Filter:
    name: str
    values: tuple[str, ...]

    def html_table_row(self) -> str:
        return tag.div(
            tag.div(self.name),
            *(
                tag.button(
                    value,
                    onclick="filter(this)",
                    class_="highlight" if value == "All" else None,
                )
                for value in self.values
            ),
        )


def generate_filter_table(filters: Iterable[Filter]) -> str:
    return tag.div(
        *(f.html_table_row() for f in filters),
        style={"overflow-x": "auto"},
        id="filters",
    )


def get_filters(path: Path) -> Iterator[Filter]:
    filter_lines = path.read_text().splitlines()
    for line in filter_lines:
        if not line:
            continue
        name, values = line.split("=")
        yield Filter(name, ("All", *values.split(",")))


def load_data(
    path: Path, sep="~", ignore_number_col=False
) -> tuple[list[str], list[list[str]]]:
    data_lines = [
        [
            x.strip('"')
            for x in (line.lstrip(string.digits) if ignore_number_col else line)
            .strip(",")
            .split(sep)
        ]
        for line in path.read_text().splitlines()
    ]

    while [""] in data_lines:
        data_lines.remove([""])

    return data_lines[0], data_lines[1:]


def create_page(fields, entries, filters):
    head = tag.tr(*(tag.th(f, class_="sticky") for f in fields), id="headings")
    body = (tag.tr(*(tag.td(e) for e in entry)) for entry in entries)
    return "".join(
        (
            tag.script((LOCAL_PATH / "filter_table.js").read_text()),
            tag.br(),
            tag.br(),
            generate_filter_table(filters),
            tag.br(),
            tag.table(tag.thead(head), tag.tbody("\n".join(body), id="filterable")),
        )
    )


def create_fragment(fields, results):
    headings = tag.tr(*(tag.th(heading) for heading in fields))
    body = (tag.tr(*(tag.td(c) for c in result)) for result in results)
    return tag.table(tag.thead(headings), tag.tbody(*body))
