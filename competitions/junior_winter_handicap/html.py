from competitions import make_page
from competitions.html_gen import create_html_table, tag

from .constants import CATEGORIES, RACE_COUNT

RACES = [f"Race {x}" for x in range(1, RACE_COUNT + 1)]


def create_html_page(page_name, data, category, r, race_count, column_align=None):
    """Create an HTML page for some data."""
    races = ["League Table"] + RACES[:race_count]
    top_table = ""
    for gender in ("Boys", "Girls"):
        for c in CATEGORIES:
            cat = f"{c} {gender}"
            top_table += tag.a(
                cat,
                href=make_page.page_name(f"{r}-{cat}"),
                class_="highlight" if cat == category else None,
            )
        # Gap in top row
        if gender == "Boys":
            top_table += tag.p("", style={"background-color": "#000;"})
    top_table = tag.div(top_table, class_="buttons") + tag.br() + tag.br()
    race_table = ""
    for race in races:
        race_table += tag.a(
            race,
            href=make_page.page_name(f"{race}-{category}"),
            class_="highlight" if race == r else None,
        )
    race_table += "".join([tag.p(r) for r in RACES if r not in races])
    top_table += tag.div(race_table, class_="buttons")
    return (
        top_table
        + tag.h2(page_name)
        + create_html_table(data, column_align=column_align)
    )
