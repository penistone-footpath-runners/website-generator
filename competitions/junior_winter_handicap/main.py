import csv
import math

from competitions import make_page
from competitions.times import Time

from . import html
from .constants import CATEGORIES, GENDERS, RACE_COUNT

RACE_PAGE_HEADINGS = ["Position", "Name", "Time", "PB?", "Race Points", "Previous PB"]
MAIN_HEADS = [
    "Position",
    "Name",
    "Races Run",
    "Points",
    "All-Time PB",
    "Series Best",
    "Series Average",
]
LEAGUE_ALIGNS = ["center", "left"]

RACE_ALIGNS = ["center", "left"]


def time_mean(times):
    times = tuple(times)
    return Time(sum(times, Time(0)) / len(times))


class Runner:
    def __init__(self, name, gender, age_group, pb, pb_is_real):
        self.effective_pb = None
        self.name = name
        self.gender = gender
        self.age_group = age_group

        self.laps = {"Under 11": 1, "Under 13": 2, "Under 15": 3, "Under 17": 3}[
            self.age_group
        ]

        self.PB = Time(pb)
        self.round_pb()
        if not int(pb_is_real):
            self.PB = None

        self.races = []

    def add_race(self, race):
        """Add a new race to the runner."""
        # Make the time the actual time
        start_time = self.start_time(race["Start Time"])
        race["Time"] -= start_time

        # Add the points field to the race
        if race["Position"] == 1:
            race["Race Points"] = 12
        else:
            race["Race Points"] = max(12 - race["Position"], 1)

        # Add the PB column
        race["Previous PB"] = str(self.PB)

        # Determine if the runner has a PB
        race["PB?"] = ""
        if self.PB is None:
            self.PB = race["Time"]
            self.round_pb()

        elif race["Time"] < self.PB:
            self.PB = race["Time"]
            self.round_pb()
            race["Race Points"] += 10
            race["PB?"] = "Yes"

        self.races.append(race)

    def get_average(self):
        """Find the runner's average time this year."""
        if self.races:
            return time_mean(
                race["Time"] for race in self.races if race["Time"] != Time("59:59")
            ).ceil()
        return None

    def get_best(self):
        """Find the best time the runner has achieved this year."""
        if self.races:
            return min(race["Time"] for race in self.races)
        return None

    def get_points(self):
        """Calculate the number of points the runner has got in total."""
        return sum(race["Race Points"] for race in self.races)

    def round_pb(self):
        """Round the PB to the nearest five seconds."""
        self.effective_pb = Time(math.ceil(self.PB.time / 5) * 5)

    def start_time(self, start_time):
        """Calculate the race's start time."""
        if not start_time:
            return worst_handicap[-1] - self.effective_pb + (self.laps - 3) * 90
        return start_time


def csv_to_dict_table(csv_file, headings):
    """Convert a CSV string to a list of dicts."""
    return [dict(zip(headings, row)) for row in csv.reader(csv_file)]


def dict_table_to_table(table, headings):
    return [headings] + [[row[heading] for heading in headings] for row in table]


def make_pages(input_path, path_to_html, year):
    global worst_handicap
    path_to_html /= f"junior-winter-handicap-{year}"
    handicap_input = input_path / "JuniorWinterHandicap" / str(year)
    writer = make_page.Writer(
        "PFR Junior Handicap", f"{year} Junior Winter Handicap", path_to_html
    )

    # Load the runner data
    with open(handicap_input / "runners.csv") as f:
        runner_data = list(csv.reader(f))

    runners = {}

    for row in runner_data:
        # Remove 'year' column as it's useless
        row.pop(2)
        # Convert the rows to Runner objects
        runners[row[0]] = Runner(*row)
        worst_handicap = [max(runner.effective_pb for runner in runners.values())]

    race_files = [None]

    for race in range(1, RACE_COUNT + 1):
        try:
            with open(handicap_input / "results" / f"{race}.csv") as f:
                race_files.append(
                    csv_to_dict_table(f, ("Rank", "Name", "Time", "Start Time"))
                )
        except FileNotFoundError:
            break

    for race in range(1, RACE_COUNT + 1):
        if race >= len(race_files):
            break
        r = f"Race {race}"
        racedata = race_files[race]
        positions = {"F": {}, "M": {}}
        for row in racedata:
            # Make times proper times
            row["Time"] = Time(row["Time"])
            # Add age category column
            runner = runners[row["Name"]]
            row["Gender"] = runner.gender
            row["Age Group"] = runner.age_group
            # Calculate positions
            try:
                positions[row["Gender"]][row["Age Group"]] += 1
                position = positions[row["Gender"]][row["Age Group"]]
            except KeyError:
                position = 1
                positions[row["Gender"]][row["Age Group"]] = 1
            row["Position"] = position

            # Add the race to the corresponding runner
            runner.add_race(row)
            # Use this to get average and best times
            row["Series Best"] = runner.get_best()
            row["Average"] = runner.get_average()

        for gender in GENDERS:
            for category in CATEGORIES:
                cat = f"{category} {GENDERS[gender]}"
                cat_data = (
                    x
                    for x in racedata
                    if x["Gender"] == gender and x["Age Group"] == category
                )
                race_name = f"{r} – {cat}"
                table = dict_table_to_table(cat_data, RACE_PAGE_HEADINGS)
                race_file = html.create_html_page(
                    race_name,
                    table,
                    cat,
                    r,
                    len(race_files) - 1,
                    column_align=RACE_ALIGNS,
                )
                writer.write(
                    f"{make_page.page_name(race_name)}",
                    race_file.replace("59:59", "None"),
                )

        worst_handicap.append(max(x.effective_pb for x in runners.values()))

    # Create league tables
    table = []
    for runner in runners.values():
        table.append(
            [
                "",
                runner.name,
                len(runner.races),
                runner.get_points(),
                runner.PB,
                runner.get_best(),
                runner.get_average(),
                runner.gender,
                runner.age_group,
            ]
        )

    # Filter out those with no races
    table = [x for x in table if x[2] > 0]

    for gender in GENDERS:
        for category in CATEGORIES:
            cat = f"{category} {GENDERS[gender]}"
            race_name = f"League Table – {cat}"
            # Filter to only those with the same group
            filtered = [x for x in table if x[7] == gender and x[8] == category]
            # Sort by PB, races, then points
            filtered.sort(key=lambda x: x[4])
            filtered.sort(key=lambda x: x[2], reverse=True)
            filtered.sort(key=lambda x: x[3], reverse=True)
            for runner in range(len(filtered)):
                # Add the position
                if (
                    filtered[runner][3] == filtered[runner - 1][3]
                    and filtered[runner] is not filtered[runner - 1]
                ):
                    filtered[runner][0] = "="
                else:
                    filtered[runner][0] = str(runner + 1)
                # Remove the gender and category fields, as they shouldn't
                # be in the final table; they were just needed for
                # filtering.
                filtered[runner] = filtered[runner][:-2]
            race_file = html.create_html_page(
                race_name,
                [MAIN_HEADS] + filtered,
                cat,
                "League Table",
                len(race_files) - 1,
                column_align=LEAGUE_ALIGNS,
            )
            writer.write(
                f"{make_page.page_name(race_name)}", race_file.replace("59:59", "None")
            )

    # Make the next-race handicaps
    start_times = []
    for runner in runners.values():
        start_times.append([runner.name, runner.laps, runner.start_time(False)])

    start_times.sort(key=lambda x: x[2])

    timespage = html.create_html_page(
        "Start Times",
        [["Name", "Laps", "Start Times"]] + start_times,
        "Start Times",
        "Start Times",
        0,
    )
    writer.write("start-times", timespage.replace("59:59", "None"))
