MILE = 1.609344
MONTHS = (
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
)

SEXES = {"F": "Female", "M": "Male"}
