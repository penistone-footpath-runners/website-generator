from competitions import html_gen
from competitions.html_gen import create_html_table, tag

PAGES = (
    ("Runners", "index.html"),
    ("Standards", "standards-list"),
    ("Information", "https://pfrac.co.uk/about/road-running"),
)

MF_HEAD = tag.div(*(tag.a(p, href=h) for p, h in PAGES), class_="buttons")
COLOURS = ("#FFFFFF", "#ffD700", "#C0C0C0", "#A87900", "#4682B4")
STANDARDS = [None, "1st Class", "2nd Class", "3rd Class", "4th Class"]


def create_html_page(
    page_name, data, titles, heading_row=0, column_align=None, highlight=()
):
    """Create an HTML page for some data."""
    head = MF_HEAD + tag.h2(
        page_name + (f" – {STANDARDS[titles[0][1]]}" if titles[0][1] else "")
    )

    head += html_gen.runner_pages_button(page_name)
    col_cols = [
        [COLOURS[0 if x == "" else STANDARDS.index(x)] for x in i[4]] for i in data
    ]

    mts = []
    for x, cat_data in enumerate(data):
        table = create_html_table(
            cat_data,
            heading_row,
            column_align,
            highlight,
            col_cols=col_cols[x],
            table_css={"table-layout": "fixed"},
        )
        mts.append(
            tag.br()
            + tag.h3(titles[x][0], style={"color": COLOURS[titles[x][1]]})
            + table
        )
    if data[0] == [
        ("5K", "10K", "10M", "HM", "20M", "Mar"),
        ["", "", "", "", "", ""],
        ["", "", "", "", "", ""],
        ["", "", "", "", "", ""],
        ["", "", "", "", "", ""],
    ]:
        cross_site = tag.em(
            f"{page_name} has done no races that qualify"
            " them for a standard in their current age group."
        )
    else:
        cross_site = mts[0][4:]
    mts.insert(0, head)
    return "".join(mts), cross_site


def create_html_list(data, linkables, heading_row=0, column_align=None, highlight=()):
    """Create an HTML page for some data."""
    head = MF_HEAD + (
        tag.h2("List of Runners")
        + tag.p(
            "Note: List shows standard achieved in current age group."
            " Click on any name to see details of races and past achievements."
        )
    )
    mts = create_html_table(
        data, heading_row, column_align, highlight, linkables=linkables
    )
    return head + mts
