from pathlib import Path
from typing import NamedTuple

from competitions.constants import MILE

MODE = "linear"

DISTANCES = {
    "5K": 5,
    "10K": 10,
    "10M": 10 * MILE,
    "HM": 42.195 / 2,
    "20M": 20 * MILE,
    "Mar": 42.195,
}

CATEGORIES = {
    "Senior": 25,
    "40–44": 40,
    "45–49": 45,
    "50–54": 50,
    "55–59": 55,
    "60–64": 60,
    "65–69": 65,
    "70–74": 70,
    "75–79": 75,
    "80+": 80,
}

PERCENTS = [x / 100 for x in (75, 65, 55, 45)]

LOCAL_PATH = Path(__file__).resolve().parent


class Point(NamedTuple):
    """A point, with x and y values."""

    x: float
    y: float


def linear(points, x):
    a, b = points
    return a.y * (x - b.x) / (a.x - b.x) + b.y * (x - a.x) / (b.x - a.x)


def cubic(points, x):
    a, b, c, d = points
    return (
        (a.y * (x - b.x) * (x - c.x) * (x - d.x))
        / ((a.x - b.x) * (a.x - c.x) * (a.x - d.x))
        + (b.y * (x - a.x) * (x - c.x) * (x - d.x))
        / ((b.x - a.x) * (b.x - c.x) * (b.x - d.x))
        + (c.y * (x - a.x) * (x - b.x) * (x - d.x))
        / ((c.x - a.x) * (c.x - b.x) * (c.x - d.x))
        + (d.y * (x - a.x) * (x - b.x) * (x - c.x))
        / ((d.x - a.x) * (d.x - b.x) * (d.x - c.x))
    )


def floor(value, interval=1):
    return interval * (value // interval)


def round_time(time):
    if time < 1800:
        return floor(time, 15)
    if time < 3600:
        return floor(time, 30)
    return floor(time, 60)


def load_factors():
    factors = [
        row.split(",") for row in (LOCAL_PATH / "factors.csv").read_text().splitlines()
    ]

    while [""] in factors:
        factors.remove([""])

    # Remove the useless distance and formatted time rows
    factors.remove(factors[0])
    factors.remove(factors[2])

    halfway = len(factors[0]) // 2

    factor_distances = [float(x) for x in factors.pop(0)[1:halfway]]
    open_seconds = factors.pop(0)
    open_seconds.remove("OC sec")
    open_seconds.remove("OC sec")
    open_seconds = [int(x) for x in open_seconds]
    open_seconds = {"F": open_seconds[: halfway - 1], "M": open_seconds[halfway - 1 :]}

    # Split the table into genders
    gender_factors = {
        "F": [row[:halfway] for row in factors],
        "M": [row[halfway:] for row in factors],
    }
    factors = gender_factors

    # Turn the tables into age: times dictionaries and convert to numbers
    for g in factors:
        factors[g] = {int(row[0]): [float(x) for x in row[1:]] for row in factors[g]}

    # Make the new table
    t = {}
    for g in factors:
        gd = {}
        for c in CATEGORIES:
            cd = {}
            for d in DISTANCES:
                if DISTANCES[d] in factor_distances:
                    index = factor_distances.index(DISTANCES[d])
                    hundred = open_seconds[g][index] / factors[g][CATEGORIES[c]][index]
                else:
                    for i in range(len(factor_distances)):
                        if factor_distances[i] > DISTANCES[d]:
                            break
                    either_side = {"linear": 1, "cubic": 2}[MODE]
                    while i + either_side >= len(factor_distances):
                        i -= 1
                    interpolate_func = {"linear": linear, "cubic": cubic}[MODE]
                    surrounders = list(range(i - either_side, i + either_side))
                    points = [
                        Point(factor_distances[i], open_seconds[g][i])
                        for i in surrounders
                    ]
                    real_os = interpolate_func(points, DISTANCES[d])

                    points = [
                        Point(factor_distances[i], factors[g][CATEGORIES[c]][i])
                        for i in surrounders
                    ]
                    real_factor = interpolate_func(points, DISTANCES[d])
                    hundred = real_os / real_factor

                cd[d] = [int(round_time(hundred / p)) for p in PERCENTS]
            gd[c] = cd
        t[g] = gd
    return t


new_standards = load_factors()


def standard(gender, category, distance, time):
    vals = new_standards[gender][category][distance]
    # To test for errors in making the data; can probably be deleted later
    assert sorted(vals) == vals

    return sorted([time] + vals).index(time) + 1
