import csv
import json
import lzma
import xml.etree.ElementTree as ET
from collections.abc import Iterable
from contextlib import suppress
from dataclasses import InitVar, dataclass
from datetime import date
from pathlib import Path

import requests
from tqdm import tqdm

from competitions import make_page, read_members, runner_pages
from competitions.constants import MONTHS, SEXES
from competitions.html_gen import tag

from . import html, standards2
from .standards2 import CATEGORIES


def get_runner_names(filename):
    members = read_members.MembersList(filename).get_runners(
        ("Power of 10 ID", "System name", "Sex", "Date of birth", "Member since")
    )
    return {
        row.power_of_10_id: (
            row.system_name,
            row.sex[0],
            date.fromisoformat(row.date_of_birth),
            date.fromisoformat(row.member_since),
        )
        for row in members
        if row.power_of_10_id and row.member_since
    }


def get_additionals(filename):
    with filename.open() as f:
        return [
            [
                row["Name"],
                row["Event"],
                row["Time"],
                row["Race"],
                uk_to_date(row["Date"]),
            ]
            for row in csv.DictReader(f)
        ]


LOCAL_PATH = Path(__file__).resolve().parent
AGES = tuple((LOCAL_PATH / "ages.txt").read_text().strip("\n").splitlines())

DATE = date.today()

LEVELS = ("1st Class", "2nd Class", "3rd Class", "4th Class")
STANDARDS = ("None",) + LEVELS
SHORT_MONTHS = {m[:3]: i for i, m in enumerate(MONTHS, start=1)}
GOOD_COLUMNS = (0, 1, 4, 10, 11)
VALID_EVENTS = tuple(standards2.DISTANCES)
ALLOWED_EVENTS = {"5000": "5K", "10000": "10K"}
# Find the list of Parkruns that are allowed
VALID_PARKRUNS = set((LOCAL_PATH / "parkruns.txt").read_text().splitlines())
VALID_PARKRUNS.discard("")

MAIN_URL = "https://www.thepowerof10.info/athletes/"
RUNNER_URL = f"{MAIN_URL}profile.aspx?athleteid="
RUN_TABLE_START = (
    '<table width="100%" cellspacing="0" cellpadding="2"'
    ' rules="none" class="alternatingrowspanel">'
)
BAD_ROW_COL = '<tr style="background-color:DarkGray;">'

TIMEOUT = 30


@dataclass
class Runner:
    name: str
    gender: str
    data: list
    date_of_birth: date
    date_joined: date
    year: int
    ignore_after: InitVar[date] = DATE
    additionals: InitVar[Iterable] = ()
    removables: InitVar[Iterable] = ()
    get_records: InitVar[bool] = False

    def __post_init__(self, ignore_after, additionals, removables, get_records):
        """Create a new runner."""
        self.data += [row for name, *row in additionals if name == self.name]
        removes = [row for name, *row in removables if name == self.name]
        self.data = [race.copy() for race in self.data if race not in removes]

        self.category = age_to_category(self.age_at(DATE))
        # Convert all of the dates and add an age (category) column
        for run in self.data:
            run.append(age_to_category(self.age_at(run[3])))

        self.data = [
            row for row in self.data if self.date_joined <= row[3] <= ignore_after
        ]

        # Find only the best performances
        new_data = []
        bests = {}
        for run in self.data:
            # Some rows say this for some reason
            if run[1] == "TBC":
                continue
            info = (run[0], run[4])  # Distance and age cat
            try:
                if info not in bests or bests[info] > time_to_secs(run[1]):
                    bests[(run[0], run[4])] = time_to_secs(run[1])
                    new_data.append(run)
            except ValueError:
                if get_records:
                    continue
                raise
        self.data = new_data

        # Calculate the standard for each run
        if get_records:
            return
        for run in self.data:
            run.append(
                standards2.standard(self.gender, run[4], run[0], time_to_secs(run[1]))
            )
            # To show rare 5th(!) standards
            # if (
            #     standards2.Standard(self.gender, run[4], run[0], time_to_secs(run[1]))
            #     == 5
            # ):
            #     print(f"Runner: {name}\nData: {run}")

    def age_at(self, date_: date, /) -> int:
        """Return the runner's age on a particular date."""
        return (
            date_.year
            - self.date_of_birth.year
            - (
                (date_.month, date_.day)
                < (self.date_of_birth.month, self.date_of_birth.day)
            )
        )

    def calculate_standards(self):
        """Return the standards that the runner has achieved."""
        standards = {}
        for run in self.data:
            # 5th class is not a thing, so it is excluded
            if run[5] < 5:
                if run[4] not in standards:
                    standards[run[4]] = {}
                standards[run[4]][run[0]] = run
        try:
            self.grade = achieved_grade(
                [x[5] for x in standards[self.category].values()]
            )
        except KeyError:
            self.grade = 0
        return standards


def achieved_grade(grades):
    """Find the grade that the runner has achieved from all of their races."""
    for grade in range(1, 5):
        if sum(grades.count(i) for i in range(1, grade + 1)) >= 3:
            return grade
    return 0


def age_to_category(age: int) -> str:
    """Convert from age to age category."""
    if age >= 80:
        return "80+"
    return AGES[age]


def uka_to_date(date_, /, current_year):
    """Parse a UKA date."""
    day, month, year = date_.split(" ")
    day = int(day)
    month = SHORT_MONTHS[month]
    year = 2000 + int(year)
    if year > current_year:
        year -= 100
    return date(year, month, day)


def uk_to_date(date_: str, /) -> date:
    """Parse a UK date."""
    day, month, year = date_.split("/")
    return date(int(year), int(month), int(day))


def time_to_secs(time):
    """Convert a time string to seconds."""
    val = sum(float(x) * 60**i for i, x in enumerate(time.strip().split(":")[::-1]))
    if val.is_integer():
        return int(val)
    return val


def format_time(secs: int) -> str:
    """Format a time nicely."""
    if secs < 6000:
        return f"{secs // 60}:{secs % 60:02}"
    hours, secs = divmod(secs, 3600)
    return f"{hours}:{secs // 60:02}:{secs % 60:02}"


def parse_page(page):
    """Extract a table from the UKA page."""
    # Reduce down to the table
    page = page[page.find(RUN_TABLE_START) + len(RUN_TABLE_START) :]
    page = tag.table(page[: page.find("</table")])

    # Delete all of the <a> misusing year-headings
    while BAD_ROW_COL in page:
        loc = page.find(BAD_ROW_COL)
        rest_of_table = page[loc:]
        page = page.replace(rest_of_table[: rest_of_table.find("</tr>") + 5], "")

    # The space breaks parser for some reason
    page = page.replace("nowrap ", "")

    # Ampersands break parser
    page = page.replace("&", "&amp;")

    # Parse the table and convert it into a nicer format
    root = ET.fromstring(page)
    table = [[y.text for y in x] for x in root]

    # Remove heading rows
    table = [[x[y] for y in GOOD_COLUMNS] for x in table if x != [None] * 12]

    # Make the time the minimum of the time rows (for chip time)
    for row in table:
        with suppress(ValueError):
            row.remove(None)
        if len(row) == 4:
            continue
        time1 = time_to_secs(row[1])
        if time_to_secs(row[2]) > time1:
            row.remove(row[2])
        else:
            row.remove(row[1])
    return table


def reduce_to_parkrun(text):
    return text[: text.find("parkrun") - 1]


class CouldNotHandleRunner(Exception):
    pass


def get_runner_data(runner_id: str) -> list[list]:
    try:
        return parse_page(requests.get(RUNNER_URL + runner_id, timeout=TIMEOUT).text)
    except Exception as e:
        raise CouldNotHandleRunner(f"could not handle {runner_id}") from e


def make_pages(input_path, path_to_html, current_year, dl, get_records=False):
    path_to_html /= "road-standards"
    standards_input = input_path / "ClubStandards/"

    writer = make_page.Writer(
        "PFR Road Standards",
        "Road Standards",
        path_to_html,
        head=tag.style(".buttons{max-width:32rem}"),
    )
    runner_names = get_runner_names(input_path / "members.csv")

    if dl:
        # Download all runner files
        pages = {athlete: get_runner_data(athlete) for athlete in tqdm(runner_names)}
        with lzma.open(standards_input / "saved_pages.json.xz", "wt") as f:
            json.dump(pages, f, separators=(",", ":"))
    else:
        with lzma.open(standards_input / "saved_pages.json.xz", "rt") as f:
            pages = json.load(f)

    additionals = get_additionals(
        standards_input / "Additional races for club standards.csv"
    )
    removables = get_additionals(
        standards_input / "Illegal races for club standards.csv"
    )
    for runner in pages:
        # Create a runner from filtering the table that parse_page produces,
        # getting rid of events which do not count
        pages[runner] = [
            x
            for x in pages[runner]
            if x[0] in VALID_EVENTS + tuple(ALLOWED_EVENTS)
            or (x[0] == "parkrun" and reduce_to_parkrun(x[2]) in VALID_PARKRUNS)
            or get_records
        ]
        for run in pages[runner]:
            if run[0] in ALLOWED_EVENTS:
                run[0] = ALLOWED_EVENTS[run[0]]
            elif run[0] == "parkrun":
                run[0] = "5K"

            run[3] = uka_to_date(run[3], DATE.year)

    # Create the list of runner objects
    runners = []
    named_pages = {}
    for runner, theirdata in runner_names.items():
        if theirdata[0] is not None:
            r = Runner(
                theirdata[0],
                theirdata[1],
                pages[runner],
                theirdata[2],
                theirdata[3],
                current_year,
                additionals=additionals,
                removables=removables,
                get_records=get_records,
            )
            named_pages[theirdata[0]] = pages[runner]
            runners.append(r)

    # Get some records data
    if get_records:
        records = {}
        flat = [
            [r.name, r.gender] + x
            for r in runners
            for x in r.data
            if r.age_at(x[3]) >= 20 and "parkrun" not in x[2]
        ]
        for g in SEXES:
            for c in CATEGORIES:
                records[(g, c)] = {}
                for row in flat:
                    if (
                        row[6] == c
                        and row[1] == g
                        and (
                            row[1] not in records[(g, c)]
                            or time_to_secs(row[3])
                            < time_to_secs(records[(g, c)][row[2]][3])
                        )
                    ):
                        records[(g, c)][row[2]] = row

        return records

    runner_fragments = {}
    linked_runners = []
    for runner in runners:
        # Figure out their standards
        stds = runner.calculate_standards()
        # Do not create a page for runners with no standards
        if not stds:
            continue
        linked_runners.append(runner.name)
        # This variable stores the actual tables to be created
        all_tables = []
        # This stores the little titles which go above the tables,
        # and their qualifications
        titles = []
        cats = list(stds)
        if runner.category not in cats:
            cats.append(runner.category)
            stds[runner.category] = {}
        cats.sort(key=(AGES + ("80+",)).index, reverse=True)
        for category in cats:
            # Create the table template
            table = [VALID_EVENTS, [], [], [], []]
            for dist in VALID_EVENTS:
                if dist in stds[category]:
                    # Add the column values
                    table[1].append(stds[category][dist][2])
                    table[2].append(stds[category][dist][3])
                    table[3].append(stds[category][dist][1])
                    table[4].append(STANDARDS[stds[category][dist][5]])

                else:
                    # Empty column for bad ones
                    for i in range(1, 5):
                        table[i].append("")
            # Add this table to the list
            all_tables.append(table)
            # Add the title, with its colour
            titles.append(
                (category, achieved_grade([x[5] for x in stds[category].values()]))
            )

        # Create and write the file
        table, cross_site = html.create_html_page(
            runner.name, all_tables, titles, column_align=["center"]
        )
        writer.write(make_page.page_name(f"{runner.name}"), table)
        runner_fragments[runner.name] = cross_site

    runner_pages.provide(input_path, "Road Standards", runner_fragments)

    # Make the list
    table = [
        [
            i.name,
            i.category,
            (
                STANDARDS[i.grade]
                if i.grade != 0 or i.category not in i.calculate_standards()
                else f"None ({len(i.calculate_standards()[i.category])}/3)"
            ),
        ]
        for i in runners
        if i.name in linked_runners
    ]

    table = sorted(table, key=lambda x: (x[0].split(" ")[1], x))
    writer.write(
        "index",
        html.create_html_list(
            [["Name", "Category", "Current Class"]] + table,
            [make_page.page_name(r) for r in linked_runners],
        ),
    )

    # Make the big list
    for year in range(2010, current_year + 1):
        fakerunners = [
            Runner(
                r.name,
                r.gender,
                named_pages[r.name],
                r.date_of_birth,
                r.date_joined,
                current_year,
                date(year, 12, 31),
                additionals=additionals,
                removables=removables,
            )
            for r in runners
        ]
        table = [
            (
                [i.name]
                + [
                    (
                        STANDARDS[
                            achieved_grade(
                                [d[5] for d in i.calculate_standards()[x].values()]
                            )
                        ]
                        if x in i.calculate_standards()
                        else ""
                    )
                    for x in CATEGORIES
                ]
                if i.name in linked_runners
                else None
            )
            for i in fakerunners
        ]
        while None in table:
            table.remove(None)
        table = sorted(table, key=lambda x: x[0].split(" ")[::-1])
        writer.write(
            f"index-{year}",
            html.create_html_list(
                [("Name",) + tuple(CATEGORIES)] + table, linked_runners
            ),
        )

    # Create the standards page
    page = html.MF_HEAD + tag.h2("Current Standards")
    for category in CATEGORIES:
        for gender in SEXES:
            table = [[""] + list(VALID_EVENTS)] + [
                [LEVELS[level]]
                + [
                    format_time(
                        standards2.new_standards[gender][category][distance][level]
                    )
                    for distance in VALID_EVENTS
                ]
                for level in range(4)
            ]
            page += tag.h3(f"{SEXES[gender]} {category}")
            page += html.create_html_table(
                table,
                sticky=False,
                heading_column=0,
                col_rows=html.COLOURS,
                table_css={"table-layout": "fixed"},
            )

    writer.write("standards-list", page)
    return None
