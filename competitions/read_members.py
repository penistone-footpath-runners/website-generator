import collections
import csv


class MembersList:
    def __init__(self, filename):
        with open(filename, encoding="latin-1") as f:
            members = csv.reader(f)
            self.members = [list(row) for row in members]
            self.headings = self.members.pop(0)

    def get_runners(self, columns):
        return_type = collections.namedtuple(
            "members_list", [c.replace(" ", "_").casefold() for c in columns]
        )
        return [
            return_type(*(row[self.headings.index(c)] for c in columns))
            for row in self.members
        ]

    @property
    def runner_names(self) -> frozenset[str]:
        return frozenset(r.system_name for r in self.get_runners(("System name",)))
