from __future__ import annotations

import math


class Time:
    def __init__(self, time: str | Time | float, /):
        if isinstance(time, str):
            self.time = self.secs_from_string(time)
        elif isinstance(time, Time):
            self.time = time.time
        elif isinstance(time, float | int):
            self.time = time
        else:
            raise TypeError(
                f"{type(self).__name__}() argument must be a string,"
                f" time or number, not '{type(time).__name__}'"
            )

    @staticmethod
    def secs_from_string(time: str) -> float:
        """Find a time from a string."""
        if time == "":
            return 0
        negative = time[0] in "-\N{MINUS SIGN}"
        if negative or time[0] == "+":
            time = time[1:]
        value = sum(
            int(x) * 60**i
            for i, x in enumerate(time.strip().split(":")[::-1])
            if x != "00"
        )
        return -value if negative else value

    def ceil(self) -> Time:
        return Time(math.ceil(self.time))

    def round(self) -> Time:
        return Time(round(self.time))

    def __format__(self, spec: str) -> str:
        """Make a string out of the time."""
        negative = self.time < 0
        time = self.time
        if negative:
            time *= -1
        time = round(time)
        if time < 6000:
            mins, secs = divmod(time, 60)
            string = f"{mins}:{secs:02}"
        else:
            hours, time = divmod(time, 3600)
            mins, secs = divmod(time, 60)
            string = f"{hours}:{mins:02}:{secs:02}"

        if "00" in spec:
            while len(string) < 8:
                string = ("0" if (len(string) + 1) % 3 else ":") + string
        if negative:
            string = "-" if "a" in spec else "\N{MINUS SIGN}" + string
        elif "+" in spec:
            string = "+" + string
        return string

    def __add__(self, time: Time) -> Time:
        return Time(self.time + Time(time).time)

    def __sub__(self, time: Time) -> Time:
        return Time(self.time - Time(time).time)

    def __mul__(self, amount: float) -> Time:
        return Time(self.time * amount)

    def __truediv__(self, amount: float) -> Time:
        return Time(self.time / amount)

    def __lt__(self, time: Time) -> bool:
        return self.time < Time(time).time

    def __le__(self, time: Time) -> bool:
        return self.time <= Time(time).time

    def __eq__(self, time: object) -> bool:
        if not isinstance(time, Time):
            return NotImplemented
        return self.time == time.time

    def __ne__(self, time: object) -> bool:
        if not isinstance(time, Time):
            return NotImplemented
        return self.time != Time(time).time

    def __ge__(self, time: Time) -> bool:
        return self.time >= Time(time).time

    def __gt__(self, time: Time) -> bool:
        return self.time > Time(time).time

    def __repr__(self) -> str:
        return f"Time('{self}')"

    def __str__(self) -> str:
        return f"{self:}"
