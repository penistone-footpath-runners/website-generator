from competitions import html_gen, make_page
from competitions.html_gen import create_html_table, tag


def section_buttons(section: str) -> str:
    return tag.div(
        tag.p(section.title()),
        tag.div(
            tag.a("F", href=f"{make_page.page_name(section)}-female"),
            tag.a("M", href=f"{make_page.page_name(section)}-male"),
            style={"display": "flex"},
        ),
        style={"display": "flex", "flex-direction": "column"},
    )


MFHEAD = (
    tag.br()
    + tag.br()
    + tag.div(
        section_buttons("Overall"),
        section_buttons("Road"),
        section_buttons("Cross Country"),
        section_buttons("Fell"),
        tag.a("Challenge", href="challenge"),
        tag.a("Information", href="https://pfrac.co.uk/competitions/club-championship"),
        tag.a("Points Calculator", href="calculator"),
        class_="buttons",
    )
)

FHEAD = (
    tag.br()
    + tag.br()
    + tag.div(
        tag.a("Female", href="fell-league-female"),
        tag.a("Male", href="fell-league-male"),
        tag.a("Races", href="races"),
        tag.a("Information", href="https://pfrac.co.uk/competitions/fell-league"),
        tag.a("Points Calculator", href="calculator"),
        class_="buttons",
    )
)


def create_html_page(
    page_name,
    data,
    heading_row=0,
    column_align=None,
    highlight=(),
    linkables=(),
    header="",
    fell=False,
    big_bold=False,
    runner=False,
):
    """Create an HTML page for some data."""
    h = FHEAD if fell else MFHEAD
    button = ""
    if runner:
        button = html_gen.runner_pages_button(page_name)
    head = (
        h
        + tag.h2(page_name)
        + button
        + tag.p(
            "Note: points are presented as integers. Hover over them to see exact"
            " values."
        )
    )
    if runner and not fell:
        head += tag.p("⋄ indicates qualified (by doing sufficient races)")
    head += header + tag.br()
    return head + tag.div(
        create_html_table(
            data,
            heading_row=heading_row,
            column_align=column_align,
            highlight=highlight,
            linkables=linkables,
            big_bold=big_bold,
            round_numbers=True,
        ),
        class_="tablediv",
    )
