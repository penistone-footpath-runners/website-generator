from .fellleague import get_category


def test_get_category():
    assert get_category("M", "2004-04-01", year=2020) == "MU17"
    assert get_category("M", "2004-10-01", year=2020) == "MU15"
    assert get_category("F", "2005-05-01", year=2020) == "FU15"
    assert get_category("M", "2020-01-01", year=2020) == "MU11"
    assert get_category("F", "1970-07-01", year=2020) == "FV45"
    assert get_category("F", "1969-07-01", year=2020) == "FV50"
    assert get_category("M", "1984-01-01", year=2020) == "MV35"
    assert get_category("F", "1985-01-01", year=2020) == "FV35"
    assert get_category("F", "2001-01-01", year=2020) == "FU20"
