"""
Functions for converting between times and strings.

Also has the points calculation function.
"""

import math


def points(winner_time, time):
    """Get the achieved number of points for a race."""
    if time <= 0:
        return 0
    if winner_time <= time:
        return 100 - 50 * math.log(winner_time / time, 0.5)
    return time / winner_time * 100


def convert_time(time):
    """Convert a time string into numerical seconds."""
    if not isinstance(time, str):
        return time
    parts = time.split(":")[::-1]
    total = 0
    b = 1
    for i in parts:
        total += float(i) * b
        b *= 60
    if "." in time:
        return total
    return int(total)


def time_format(time):
    """Format a number of seconds as a time string."""
    if isinstance(time, int):
        return f"{time} points"
    secs = int(convert_time(time))
    if secs < 6000:
        return f"{secs // 60}:{secs % 60:02}"
    hours = secs // 3600
    secs %= 3600
    return f"{hours}:{secs // 60:02}:{secs % 60:02}"
