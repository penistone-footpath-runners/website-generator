from dataclasses import dataclass

from . import points


class BadRunner(Exception):
    pass


@dataclass
class Race:
    name: str
    date: str
    category: str
    runners: list
    winners: list
    results: list

    def __post_init__(self):
        self.points_done = False
        # Deal with races with no male or female winner
        self.winner_table = [w for w in self.winners if w not in (["F"], ["M"])]

        self.winning_times = {
            i[1]: points.convert_time(i[2]) for i in self.winner_table
        }
        self.add_genders()
        self.calculate_points()
        self.format_times()
        # Format the winners' times
        for w in self.winner_table:
            w[2] = points.time_format(w[2])

    def add_genders(self):
        """Add the genders and filter the bad runners out."""
        new_results = []
        for i in range(0, len(self.results), 1):
            allowed_id = 3 if "Fell" in self.category else 2

            if not (
                self.results[i][0] in self.runners
                and self.runners[self.results[i][0]][allowed_id]
            ):
                raise BadRunner(
                    f"Invalid runner: {self.results[i][0]} (Race is {self.name})"
                )
            new_results.append(self.results[i] + [self.runners[self.results[i][0]][1]])
        self.results = new_results

    def calculate_points(self):
        """Calculate points for everyone."""
        self.results = [
            [
                *(x[:-1] if self.points_done else x),
                points.points(self.winning_times[x[3][0]], points.convert_time(x[2])),
            ]
            for x in self.results
        ]
        self.points_done = True

    def format_times(self):
        for r in self.results:
            r[2] = points.time_format(r[2])
