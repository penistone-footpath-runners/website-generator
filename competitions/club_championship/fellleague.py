import shutil
from datetime import date
from pathlib import Path

from competitions import make_page, runner_pages
from competitions.constants import SEXES
from competitions.html_gen import tag

from . import fellrunner as runner, fileio, html, language

LOCAL_PATH = Path(__file__).resolve().parent

RACE_HEADS = ["Name", "Position", "Time", "Category", "Points"]
RUNNER_HEADS = ["Race", "Date", "Position", "Time", "Points"]
CATEGORIES = (
    "MU11",
    "MU13",
    "MU15",
    "MU17",
    "MU20",
    "M Senior",
    "MV35",
    "MV40",
    "MV45",
    "MV50",
    "MV55",
    "MV60",
    "MV65",
    "MV70",
    "MV75",
    "MV80",
    "MV85",
    "MV90",
    "MV95",
    "FU11",
    "FU13",
    "FU15",
    "FU17",
    "FU20",
    "F Senior",
    "FV35",
    "FV40",
    "FV45",
    "FV50",
    "FV55",
    "FV60",
    "FV65",
    "FV70",
    "FV75",
    "FV80",
    "FV85",
    "FV90",
    "FV95",
)


def get_category(sex, date_of_birth, year):
    date_of_birth = date.fromisoformat(date_of_birth)
    age = year - date_of_birth.year - (date_of_birth >= date(date_of_birth.year, 4, 1))
    if age >= 35:
        return f"{sex}V{age // 5 * 5}"
    if age >= 20:
        return f"{sex} Senior"
    school_age = (
        year - date_of_birth.year - (date_of_birth >= date(date_of_birth.year, 9, 1))
    )
    if school_age > 17:
        return f"{sex}U20"
    if school_age < 11:
        return f"{sex}U11"
    if not school_age % 2:
        school_age += 1
    return f"{sex}U{school_age}"


RACE_ALIGN = ["left", "center", "center", "left;padding-left:2.5rem", "center"]  # HACK

PERSON_ALIGN = ["left", "center", "center", "right", "center"]

MAIN_ALIGN = ["left", "left", "left", "center", "center"]


class IllegalRunner(Exception):
    pass


def remove_columns(data, columns):
    """Remove the item at columns from each list in the list."""
    return [[cell for i, cell in enumerate(row) if i not in columns] for row in data]


def create_main(page_name, columns_to_remove, table, linkables, column_align, headings):
    """Create a main-type table."""
    new_table = remove_columns([["Position"] + headings] + table, columns_to_remove)
    return html.create_html_page(
        page_name, new_table, column_align=column_align, linkables=linkables, fell=True
    )


def move_to_html(filename, path_to_html):
    shutil.copy2(LOCAL_PATH / filename, path_to_html / filename)


def update_fell_league(input_path, path_to_html, year):
    path_to_html /= f"fell-league-{year}"
    writer = make_page.Writer(
        "PFR Fell League",
        f"{year}–{(year + 1) % 100:02} Fell League",
        path_to_html,
        head=tag.style(".buttons{max-width:50rem}"),
    )

    # Put the calculator script into that directory
    move_to_html("calculator.js", path_to_html)

    runners = fileio.get_runners(input_path, lambda *a: get_category(*a, year=year))

    linked_things = [
        make_page.page_name(p)
        for p in [x.stem for x in fileio.list_races(input_path, year)] + list(runners)
    ]
    # Get the races
    races = fileio.get_races("Fell League", input_path, runners, year)
    races.sort(key=lambda x: "".join(x.date.split("/")[::-1]))

    main_heads = ["Name", "Category", "Points", "Races"]

    # Create the race files
    for r in races:
        header = html.create_html_table(
            r.winner_table, heading_row=None, round_numbers=True
        ) + (
            tag.p("")
            + tag.em(
                f"{len(r.results)} Penistone "
                f"{'runner' if len(r.results) == 1 else 'runners'}"
            )
            + tag.p("")
        )
        writer.write(
            make_page.page_name(r.name),
            html.create_html_page(
                r.name,
                [RACE_HEADS] + r.results,
                column_align=RACE_ALIGN,
                linkables=linked_things,
                header=header,
                fell=True,
            ),
        )

    # Create the personal data for main table creation and runner pages
    people = []
    for runner_name in runners:
        person = runner.Runner(
            runner_name, runners[runner_name][0], runners[runner_name][1]
        )
        for r in races:
            for run in r.results:
                if run[0] == runner_name:
                    person.add_result([r.name] + [r.date] + run[1:3] + [run[4]])

        if person.results:
            people.append(person)
            if not runners[runner_name][3]:
                raise IllegalRunner(runner_name)

    # Make main tables
    main_table = []
    for p in people:
        row = [p.name, p.category, p.points, p.total_races]

        main_table.append(row)

    # Create the actual main table
    nt = sorted(main_table, key=lambda x: x[2], reverse=True)
    gender_tables = {z: [x for x in nt if x[1][0] == z] for z in SEXES}
    ranks = {
        z: {b: a + 1 for a, b in enumerate(i[0] for i in gender_tables[z])}
        for z in SEXES
    }

    # Category ranks
    cat_ranks = {
        z: {b: a + 1 for a, b in enumerate(x[0] for x in nt if x[1] == z)}
        for z in CATEGORIES
    }

    # Create the runner pages
    runner_fragments = {}
    for person in people:
        name = person.name
        # Position and points
        header = (
            tag.div(
                tag.em(
                    f"{language.number_positions(ranks[person.gender][name])} overall"
                ),
                class_="column",
            )
            + tag.div(
                tag.em(
                    f"{language.number_positions(cat_ranks[person.category][name])}"
                    f" {person.category}",
                    class_="column",
                )
            )
            + tag.div(
                tag.span(
                    tag.em(language.pluralise(round(person.points), "point", "points")),
                    title=str(round(person.points, 6)),
                ),
                class_="column",
            )
            + tag.br()
        )

        # Cross-site stuff
        runner_fragments[person.name] = header

        writer.write(
            make_page.page_name(name),
            html.create_html_page(
                name,
                [RUNNER_HEADS] + person.results,
                column_align=PERSON_ALIGN,
                linkables=linked_things,
                header=header,
                fell=True,
                runner=True,
            ),
        )

    runner_pages.provide(input_path, "Fell League", runner_fragments)

    # Create the overall pages
    for g in SEXES.values():
        ttw = gender_tables[g[0]]
        # Add the position column
        ttw = [[x + 1] + ttw[x] for x in range(0, len(ttw))]

        pagename = f"Fell League – {g}"
        writer.write(
            f"fell-league-{g.casefold()}",
            create_main(pagename, [], ttw, linked_things, MAIN_ALIGN, main_heads),
        )

    races_index = [[r.name, r.date, len(r.results)] for r in races]
    races_index.sort(key=lambda x: (x[1].split("/")[::-1], x[0]))
    races_index = [["Race", "Date", "Penistone Runners"]] + races_index
    writer.write(
        "races",
        html.create_html_page("Races", races_index, linkables=linked_things, fell=True),
    )

    # Create the points calculator page
    writer.write("calculator", html.FHEAD + (LOCAL_PATH / "calccode.html").read_text())
