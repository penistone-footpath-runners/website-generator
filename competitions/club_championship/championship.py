"""Create the club championship results pages."""

import shutil
from datetime import date
from pathlib import Path

from competitions import make_page, runner_pages
from competitions.constants import SEXES
from competitions.html_gen import tag

from . import fileio, html, language, points, runner

RACE_CATS = ("Overall", "Road", "Cross Country", "Fell", "Challenge")
RACE_HEADS = ["Name", "Position", "Time", "Category", "Points"]
RUNNER_HEADS = ["Race", "Category", "Date", "Position", "Time", "Points"]
MAIN_HEADS = [
    "Name",
    "Category",
    "Overall Points",
    "Road Points",
    "Cross Country Points",
    "Fell Points",
    "Challenge Points",
    "Races",
    "Races",
    "Races",
    "Races",
]

LOCAL_PATH = Path(__file__).resolve().parent

CATEGORIES = tuple(["U20", " Senior"] + [f"V{age}" for age in range(35, 85, 5)])


def get_category(sex, date_of_birth, year):
    date_of_birth = date.fromisoformat(date_of_birth)
    age = year - date_of_birth.year - (date_of_birth >= date(date_of_birth.year, 4, 1))
    if age < 20:
        return f"{sex}U20"
    if age >= 35:
        return f"{sex}V{age // 5 * 5}"
    return f"{sex} Senior"


# Create a list of strings that can be hyperlinks
SERIES_ALIGN = ["left", "left"] + ["center"] * 10

RACE_ALIGN = ["left", "center", "center", "left;padding-left:2.5rem", "center"]

PAGE_NAMES = {
    "Road": "Road Section",
    "Cross Country": "Cross Country Section",
    "Fell": "Fell Section",
    "Overall": "Overall Championship",
    "Challenge": "Challenge Championship",
}

PERSON_ALIGN = [
    "left",
    "left;padding-left:1.25rem",
    "center",
    "center",
    "center",
    "center",
]


class IllegalRunner(Exception):
    pass


def remove_columns(data, columns):
    """Remove the item at columns from each list in the list."""
    new_data = []
    for row in data:
        newrow = []
        for i, cell in enumerate(row):
            if i not in columns:
                newrow.append(cell)
        new_data.append(newrow)
    return new_data


def create_main(
    page_name, columns_to_remove, table, linkables, column_align, headings, highlight
):
    """Create a main-type table."""
    new_table = [["Position"] + headings] + table

    new_table = remove_columns(new_table, columns_to_remove)
    highlight = remove_columns(highlight, columns_to_remove)
    return html.create_html_page(
        page_name,
        new_table,
        column_align=column_align,
        highlight=highlight,
        linkables=linkables,
    )


def index_by_index(array, index, value):
    return next(i for i, v in enumerate(array) if v[index] == value)


def move_to_html(filename, path_to_html):
    shutil.copy2(LOCAL_PATH / filename, path_to_html / filename)


def update_championship(input_path, path_to_html, year):
    path_to_html /= f"championship-{year}"

    writer = make_page.Writer(
        "PFR Club Championship",
        f"{year}–{(year + 1) % 100:02} Club Championship",
        path_to_html,
        head=tag.style(".buttons{max-width:50rem}"),
    )
    runners = fileio.get_runners(input_path, lambda s, d: get_category(s, d, year))

    # Put the calculator script into that directory
    move_to_html("calculator.js", path_to_html)

    # Get the races
    races = fileio.get_races("Championship", input_path, runners, year)
    races.sort(key=lambda x: "".join(x.date.split("/")[::-1]))

    # Create the event_names list
    event_names = []
    cat_races = {"Road": [], "Cross Country": [], "Fell": []}

    serieses = {}
    for race in races:
        if "Series" in race.category:
            category = race.category.removesuffix(" Series")
            rn, _, number = race.name.rpartition(" ")
            if not number.isnumeric():
                raise ValueError(f"series race without number: {race.name}")
            if rn not in event_names:
                event_names.append(rn)
            cat_races[category].append(rn)
            serieses[rn] = category
        else:
            event_names.append(race.name)
            try:
                cat_races[race.category].append(race.name)
            except KeyError:
                # Race is not part of a normal category. This is fine.
                continue

    linked_things = [
        make_page.page_name(p)
        for p in [
            *[z.stem for z in fileio.list_races(input_path, year)],
            *runners,
            *serieses,
        ]
    ]

    # Calculate the best winning times for each series
    series_times = {"F": {}, "M": {}}
    for series in serieses:
        for race in races:
            if series in race.name:
                for sex in SEXES:
                    win_time = points.convert_time(race.winning_times[sex])
                    series_times[sex].setdefault(series, win_time)
                    series_times[sex][series] = min(win_time, series_times[sex][series])

    for series in serieses:
        for race in races:
            if series in race.name:
                for sex in SEXES:
                    race.winning_times[sex] = series_times[sex][series]
                race.calculate_points()

    # Create the race files
    for r in races:
        header = (
            (
                html.create_html_table(
                    r.winner_table, heading_row=None, round_numbers=True
                )
                + tag.p("")
            )
            + tag.em(f"{len(r.results)} Penistone runners")
            + tag.p("")
        )
        writer.write(
            f"{make_page.page_name(r.name)}",
            html.create_html_page(
                r.name,
                [RACE_HEADS] + r.results,
                column_align=RACE_ALIGN,
                linkables=linked_things,
                header=header,
            ),
        )

    # Create some info for the per-person files
    people = []
    for name, (gender, category, other_eligible, fell_eligible) in runners.items():
        results = []
        for r in races:
            try:
                result = next(run for run in r.results if run[0] == name)
            except StopIteration:
                continue
            if (
                r.category == "Fell"
                and fell_eligible
                or r.category != "Fell"
                and other_eligible
            ):
                results.append((r.name, r.category, r.date, *result[1:3], result[4]))
            else:
                raise IllegalRunner(f"Illegal runner for {r}: {name}")
        if results:
            people.append(runner.Runner(name, gender, category, results))

    # Make main tables
    main_table = []
    counter_lists = {
        "Challenge": [],
        "Road": [],
        "Cross Country": [],
        "Fell": [],
        "Overall": [],
    }

    for p in people:
        row = [
            p.name,
            p.category,
            p.overall_points,
            p.points["Road"],
            p.points["Cross Country"],
            p.points["Fell"],
            p.challenge_points,
            p.total_races,
            p.race_counts["Road"],
            p.race_counts["Cross Country"],
            p.race_counts["Fell"],
        ]
        for r in event_names:
            for result in p.results:
                if r == result[0]:
                    row.append(result[5])
                    break
                if r in result[0]:
                    # [:-7] removes 'series'
                    result = p.series_scores[result[1][:-7]].get(r)
                    if result is None:
                        row.append("")
                    else:
                        row.append(result)
                    break
            else:
                row.append("")
        for category in PAGE_NAMES:
            if p.qualified[category]:
                counter_lists[category].append(p.name)

        main_table.append(row)

    main_heads = MAIN_HEADS + event_names

    tables = [
        ("Overall", [4, 5, 6, 7, 9, 10, 11]),
        (
            "Road",
            [3, 5, 6, 7, 8, 10, 11]
            + [
                None if r in cat_races["Road"] else main_heads.index(r) + 1
                for r in event_names
            ],
        ),
        (
            "Cross Country",
            [3, 4, 6, 7, 8, 9, 11]
            + [
                None if r in cat_races["Cross Country"] else main_heads.index(r) + 1
                for r in event_names
            ],
        ),
        (
            "Fell",
            [3, 4, 5, 7, 8, 9, 10]
            + [
                None if r in cat_races["Fell"] else main_heads.index(r) + 1
                for r in event_names
            ],
        ),
        ("Challenge", [2, 3, 4, 5, 6, 9, 10, 11]),
    ]

    # Define the main table alignments
    main_align = ["left", "left", "left"]
    challenge_align = main_align[1:]

    # Create the actual main tables
    for j, (section, columns_to_remove) in enumerate(tables):
        ntd = [x for x in main_table if x[2 + j]]
        if section == "Challenge":
            align = challenge_align
            nts = [(None, sorted(ntd, key=lambda x: x[6], reverse=True))]
        else:
            align = main_align
            nts = [
                (
                    full,
                    sorted(
                        (x for x in ntd if x[1][0] == short),
                        key=lambda x: x[2 + j],
                        reverse=True,
                    ),
                )
                for short, full in SEXES.items()
            ]

        for gender, nt in nts:
            # Find qualifiers
            nt.sort(key=lambda x: x[0] in counter_lists[section], reverse=True)
            # Add the position column
            nt = [(i, *x) for i, x in enumerate(nt, start=1)]
            # Re-make, with positions
            qualified = [x for x in nt if x[1] in counter_lists[section]]

            page_name = PAGE_NAMES[section]
            if gender is not None:
                page_name += f" – {gender}"
            # Create the table
            p = create_main(
                page_name,
                columns_to_remove,
                nt,
                linked_things,
                align,
                main_heads,
                qualified,
            )

            filename = section if gender is None else f"{section}-{gender}"
            writer.write(make_page.page_name(filename), p)

    # Calculate the positions and make the runner files
    runner_fragments = {}
    for person in people:
        runner_name = person.name
        cat = person.category
        mini_table = []
        for i, section in enumerate(RACE_CATS):
            row = [section]

            if section == "Overall":
                score = language.pluralise(person.overall_points, "point", "points")
            elif section == "Challenge":
                score = language.pluralise(person.challenge_points, "point", "points")
            else:
                score = language.pluralise(person.points[section], "point", "points")

            if score == "0 points":
                row += ["", "", score]
                mini_table.append(row)
                continue

            sorted_table = sorted(
                main_table,
                key=lambda x: (x[0] in counter_lists[section], x[i + 2]),
                reverse=True,
            )

            if section == "Challenge":
                gender_filter = sorted_table
            else:
                gender_filter = [x for x in sorted_table if x[1][0] == cat[0]]
            cat_filter = [x for x in sorted_table if x[1] == cat]

            # Position
            pos = language.number_positions(
                1 + index_by_index(gender_filter, 0, runner_name)
            )
            row.append(f"{pos} overall")
            if runner_name in counter_lists[section]:
                row[-1] += " ⋄"

            # Age category position
            age_pos = language.number_positions(
                1 + index_by_index(cat_filter, 0, runner_name)
            )
            row.append(f"{age_pos} {cat}")
            if runner_name in counter_lists[section]:
                row[-1] += " ⋄"

            row.append(score)

            mini_table.append(row)

        mini_table = list(map(list, zip(*mini_table)))
        header = html.create_html_table(
            mini_table,
            column_align=["center"],
            sticky=False,
            table_css={"table-layout": "fixed"},
            round_numbers=True,
        )

        runner_fragments[person.name] = header

        writer.write(
            f"{make_page.page_name(runner_name)}",
            html.create_html_page(
                runner_name,
                [RUNNER_HEADS] + person.results,
                column_align=PERSON_ALIGN,
                highlight=person.counters,
                linkables=linked_things,
                header=header,
                runner=True,
            ),
        )

    runner_pages.provide(input_path, "Club Championship", runner_fragments)

    # Create the series tables
    for series, cat in serieses.items():
        # Find how many there are in the series
        race_count = sum(series in r.name for r in races)

        # Build the table
        table = []
        for p in sorted(
            (p for p in people if series in p.series_scores[cat]),
            key=lambda x: x.series_scores[cat].get(series),
            reverse=True,
        ):
            race_points = {
                int(result[0].replace(series + " ", "")): result[5]
                for result in p.results
                if series in result[0]
            }
            table.append(
                [p.name, p.category]
                + [race_points.get(i, "") for i in range(1, race_count + 1)]
            )

        writer.write(
            f"{make_page.page_name(series)}",
            html.create_html_page(
                series,
                [
                    ["Name", "Category"]
                    + [
                        tag.a(f"Race {x}", href=f"{make_page.page_name(series)}-{x}")
                        for x in range(1, race_count + 1)
                    ]
                ]
                + table,
                column_align=SERIES_ALIGN,
                linkables=linked_things,
                big_bold=True,
            ),
        )

    # Create the points calculator page
    calc_code = (LOCAL_PATH / "calccode.html").read_text()
    writer.write("calculator", html.MFHEAD + calc_code)

    # Make the prizes page
    prize_data = []
    for section in ("Road", "Cross Country", "Fell"):
        for gender in SEXES:
            for age_cat in CATEGORIES:
                qualified = [
                    r
                    for r in people
                    if r.category == gender + age_cat and r.qualified[section]
                ]
                ranked = sorted(
                    qualified, key=lambda r: r.points[section], reverse=True
                )
                formatted = [
                    [section, gender + age_cat, i + 1, r.name, r.points[section]]
                    for i, r in enumerate(ranked)
                ]
                prize_data += formatted[:2]

    headings = ["Section", "Category", "Position", "Name", "Points"]
    writer.write(
        "prizes",
        html.create_html_page(
            "Prizes", [headings] + prize_data, linkables=linked_things
        ),
    )
