from collections import defaultdict
from dataclasses import dataclass, field

CATEGORIES = ("Road", "Cross Country", "Fell")


@dataclass
class Runner:
    """A runner for the Championship."""

    name: str
    gender: str
    category: str
    results: list[tuple] = field(default_factory=list)

    def __post_init__(self):
        """Calculate all of the points and best results."""

        def points(row):
            return row[5]

        self.points = {}
        self.race_counts = {}
        self.counters = []
        self.qualified = {
            "Challenge": True,
            "Overall": False,
            "Road": False,
            "Cross Country": False,
            "Fell": False,
        }
        all_counters = []
        self.series_scores = {"Road": {}, "Cross Country": {}, "Fell": {}}
        for cat in CATEGORIES:
            serieses = defaultdict(list)
            runs = []
            for run in self.results:
                if cat == run[1]:
                    runs.append(run)
                elif cat in run[1]:
                    serieses[run[0][:-2]].append(run)

            # Find the best in the series
            for name, series in serieses.items():
                series.sort(key=points, reverse=True)
                runs.append(series[0])
                self.series_scores[cat][name] = series[0][5]

            self.race_counts[cat] = len(runs)
            # Find the best two and three
            runs.sort(key=points, reverse=True)

            self.counters += runs[:2]

            # Store the individual category points
            if len(runs) >= 3:
                self.qualified[cat] = True
            self.points[cat] = sum(map(points, runs[:3]))

            all_counters += runs

        for run in self.results:
            if run[1] == "Club mile":
                all_counters.append(run)
        self.challenge_points = sum(map(points, all_counters))

        # Bring the counters up to 9
        all_counters.sort(key=points, reverse=True)
        self.counters += [r for r in all_counters if r not in self.counters][:2]

        self.overall_points = sum(map(points, self.counters))
        self.total_races = len(self.results)

        if len(self.counters) == 8:
            self.qualified["Overall"] = True
