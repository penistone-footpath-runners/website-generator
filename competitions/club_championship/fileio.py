import copy

from competitions import read_members

from . import race


def parse_cell(string):
    """Convert a string to an int or float, if possible."""
    try:
        return int(string)
    except ValueError:
        try:
            return float(string)
        except ValueError:
            return string


def extract_table(text):
    """Extract a table from a string version."""
    table = [[parse_cell(y) for y in x.split(",")] for x in text.splitlines()]
    while table[-1] == [""]:
        table = table[:-1]
    return table


def get_races(allowed, path, runners, year):
    """Find all of the race data."""
    race_files = list_races(path, year)
    races = []
    for r in race_files:
        race_text = r.read_text()
        find_if_allowed = race_text.splitlines()
        good = False
        for i in find_if_allowed:
            if allowed in i and "true" in i.casefold():
                good = True
        if not good:
            continue

        race_text = race_text.splitlines()[2:]
        # Remove commas on the ends of lines
        race_text = "\n".join([x.strip(",") for x in race_text])

        metadata, results = race_text.split("\n\n")
        metadata = metadata.splitlines()
        name, date, category = metadata[:3]
        winners = extract_table("\n".join(metadata[3:]))

        results = extract_table(results)

        races.append(
            race.Race(name, date, category, runners, winners, copy.deepcopy(results))
        )
    return races


def list_races(path, year):
    """List all of the races in the races/ directory."""
    return [
        f for f in (path / "RaceResults" / str(year)).iterdir() if f.suffix == ".csv"
    ]


def get_runners(path, category_function):
    """Find the list of runners."""
    members = read_members.MembersList(path / "members.csv").get_runners(
        ("System name", "Sex", "Date of birth", "First claim other", "First claim fell")
    )
    return {
        x.system_name: (
            x.sex[0],
            category_function(x.sex[0], x.date_of_birth),
            x.first_claim_other == "yes",
            x.first_claim_fell == "yes",
        )
        for x in members
    }
