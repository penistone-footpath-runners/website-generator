class Runner:
    def __init__(self, name, gender, age_cat, results=None):
        self.name = name
        self.gender = gender
        self.category = age_cat
        if results is None:
            results = []
        self.results = results
        self.categories = ("Road", "Cross Country", "Fell")

    def add_result(self, result):
        """Add a result to the runner."""
        self.results.append(result)
        self.calculate_points_and_runs()

    def calculate_points_and_runs(self):
        """Calculate all of the points and best results."""
        self.points = sum(self.get_points_only())
        self.total_races = len(self.results)

    def get_points_only(self):
        return [x[4] for x in self.results]

    def __repr__(self):
        return (
            "Runner("
            f"{self.name!r}, {self.gender!r}, {self.category!r}, "
            f"results={self.results!r})"
        )
