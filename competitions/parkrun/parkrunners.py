from competitions import read_members


def runners(filename):
    members = read_members.MembersList(filename).get_runners(
        ("parkrun ID", "System name", "Member since")
    )
    return {
        row.parkrun_id: (row.system_name, row.member_since)
        for row in members
        if row.parkrun_id
    }
