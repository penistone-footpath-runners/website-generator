import datetime


def get_dates(year: int) -> list[str]:
    date = datetime.date(year, 1, 1)
    dates = {date, datetime.date(year, 12, 25)}
    while date < datetime.date(year + 1, 1, 1):
        if date.isoweekday() == 6:
            dates.add(date)
        date += datetime.timedelta(days=1)
    return sorted(d.isoformat() for d in dates if d <= datetime.date.today())
