"""Download data from the Parkrun website, storing it in data.txt."""

from __future__ import annotations

import json
import lzma
import re
import time
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path

import requests
from tqdm import tqdm

from .dates import get_dates

DELAY = 7


@dataclass
class Result:
    parkrun_name: str
    url: str
    position: int
    gender: str
    gender_pos: int
    age_cat: str
    club: str
    time: str
    is_pb: bool
    age_grade: str
    cat_pos: str | None
    runs: int


def load_list(filename: Path) -> list[str]:
    """Load a newline-separated list from a text file."""
    return [line for line in filename.read_text().splitlines() if line]


LOCAL_PATH = Path(__file__).resolve().parent

HEADERS = {
    "User-Agent": (
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
        " AppleWebKit/537.36 (KHTML, like Gecko)"
        " Chrome/114.0.0.0 Safari/537.36"
    )
}

RESULTS_TABLE = re.compile(
    '<table class="Results-table Results-table--compact js-ResultsTable">(.*?)</table>',
    re.DOTALL,
)
CLUB_NAME = "Penistone Footpath Runners AC"

URL = "https://www.parkrun.com/results/consolidatedclub/?clubNum=2801&eventdate="


class BlockedError(Exception):
    pass


def find_links(text: str) -> list[str]:
    """Locate the links in an HTML file for the club summary page."""
    if "403 forbidden" in text.casefold():
        raise BlockedError()
    return re.findall(r"href='(https://www.parkrun.org.uk/[^']+/results/\d+/?)'", text)


@dataclass
class Downloader:
    delay_duration: float
    delay_next: bool = False

    def delay(self) -> None:
        time.sleep(self.delay_duration)

    def get(self, url: str) -> str:
        """Get the HTML code, as a string, from a website."""
        while True:
            if self.delay_next:
                self.delay()
            self.delay_next = True
            try:
                return requests.get(url, headers=HEADERS, timeout=30).text
            except requests.exceptions.Timeout:
                print("Error: retrying")
            except requests.exceptions.RequestException as e:
                msg = f"{e}\n\nThere was an error accessing ‘{url}’."
                raise requests.exceptions.RequestException(msg) from None


def unwrap_match(re_match: re.Match[str] | None) -> str:
    if re_match is None:
        raise ValueError("no match found")
    return re_match[1]


def find_table(text: str) -> str:
    """Find the useful table and return it."""
    return f"<all>{unwrap_match(RESULTS_TABLE.search(text))}</all>"


def remove_image_tags(data: str) -> str:
    """Find and remove HTML img tags from the HTML."""
    return re.sub(r"<(?:img|link|meta|source) .*?>", "", data)


def parse(string):
    """Go through an HTML string and make a table with the wanted runners."""
    string = remove_image_tags(string)
    # Replace <br> tags with spaces
    string = string.replace("<br/>", " ").replace("<br>", " ").replace("<br />", " ")

    root = ET.fromstring(string)[1]

    table = []
    runner_count = len(root)
    for result in root:
        if result.attrib["data-name"] == "Unknown":
            continue
        try:
            gender = result.attrib["data-gender"][0]
        except IndexError:
            continue
        try:
            gender_pos = int(result[2][1].text)
        except TypeError:
            continue
        table.append(
            Result(
                parkrun_name=result.attrib["data-name"],
                url=re.search(
                    r"parkrunner/(\d+)", result[1][0][0].attrib["href"]
                ).group(1),
                position=int(result.attrib["data-position"]),
                gender=gender,
                gender_pos=gender_pos,
                age_cat=result.attrib["data-agegroup"],
                club=result.attrib["data-club"],
                time=result[5][0].text,
                is_pb=(result.attrib["data-achievement"] == "New PB!"),
                age_grade=result.attrib["data-agegrade"].replace(" %", ""),
                cat_pos=None,
                runs=int(result.attrib["data-runs"]),
            )
        )

    # Calculate age position
    runners_before = {}
    for runner in table:
        age_cat = runner.age_cat
        if age_cat in runners_before:
            runners_before[age_cat] += 1
            runner.cat_pos = runners_before[age_cat]
        # Runner is first in age category
        else:
            runner.cat_pos = 1
            runners_before[age_cat] = 1

    # Filter out the runners that are from the wrong club
    table = [r for r in table if r.club == CLUB_NAME]

    return [
        [
            r.parkrun_name,
            r.url,
            r.time,
            r.age_cat,
            r.age_grade,
            r.gender,
            r.gender_pos,
            r.is_pb,
            r.cat_pos,
            r.runs,
            runner_count,
        ]
        for r in table
    ]


def extract_results(html: str, date: str) -> list[list]:
    results = parse(find_table(html))
    header = unwrap_match(re.search('<div class="Results-header">(.*?)</div>', html))
    parkrun_name = unwrap_match(re.search("<h1>(.*?)</h1>", header)).replace(
        " parkrun", ""
    )
    num = int(unwrap_match(re.search("#(.*?)</span>", header)))
    return [[parkrun_name, num, date] + row for row in results]


def main(input_path: Path, year: int, redownload_all: bool = False) -> None:
    parkrun_path = input_path / "Parkrun"
    dates = get_dates(year)
    # Open the existing data file to see which dates have been done
    data_file = parkrun_path / "data.json.xz"
    if redownload_all or not data_file.exists():
        table = []
    else:
        with lzma.open(data_file, "rt") as f:
            table = json.load(f)

    # row[2] is the date field for each race
    done_dates = {row[2] for row in table}
    needed_dates = [d for d in dates if d not in done_dates]
    if not needed_dates:
        needed_dates = [dates[-1]]
        table = [row for row in table if row[2] != dates[-1]]
    downloader = Downloader(DELAY)
    date_links = {
        date: find_links(downloader.get(URL + date)) for date in tqdm(needed_dates)
    }
    bad_files = {}
    with tqdm(total=sum(len(v) for v in date_links.values())) as pbar:
        for date, links in date_links.items():
            for link in links:
                # Foreign parkruns do not count
                if "org.uk" not in link:
                    pbar.update(1)
                    continue
                html = downloader.get(link)
                try:
                    table += extract_results(html, date)
                except Exception:
                    bad_files[link] = html
                pbar.update(1)

    if bad_files:
        save_path = parkrun_path / "bad_file.html"
        save_path.write_text(
            "\n\n".join(
                f"Bad page: {link}\n{content}" for link, content in bad_files.items()
            )
        )
        print(
            "\n".join(
                ("Finished, with errors processing the following pages:", *bad_files)
            )
        )
        print(f"Pages saved to {save_path}.")

    with lzma.open(data_file, "wt") as f:
        json.dump(table, f, separators=(",", ":"))
