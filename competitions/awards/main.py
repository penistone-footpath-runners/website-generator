import re
from pathlib import Path

import pandas as pd

from competitions import filterable_table, make_page, read_members, runner_pages

LOCAL_PATH = Path(__file__).resolve().parent


def name_in_list(name: str, name_list: str) -> bool:
    if name in name_list:
        return True
    names = name.split(" ")
    return (
        len(names) == 2
        and names[1] in name_list
        and bool(re.search(f"{names[0]} & [^ ]+ {names[1]}", name_list))
    )


def load_2d(path: Path):
    df = (
        pd.read_table(path)
        .melt("Year")
        .rename(columns={"variable": "Award", "value": "Winner"})
    )
    df["Type"] = df.Award.map(
        {
            "Club Champion (Male)": "Championship",
            "Road Champion (Male)": "Championship",
            "XC Champion (Male)": "Championship",
            "Fell Champion (Male)": "Championship",
            "Club Champion (Female)": "Championship",
            "Road Champion (Female)": "Championship",
            "XC Champion (Female)": "Championship",
            "Fell Champion (Female)": "Championship",
            "Challenge Champion": "Championship",
            "Senior Winter Handicap": "Winter Handicap",
            "Senior WH Age Graded League": "Winter Handicap",
            "Merit Award": "Merit Award",
            "Fell League (Male)": "Fell League",
            "Fell League (Female)": "Fell League",
            "Parkrun Competition (Male)": "Parkrun Competition",
            "Parkrun Competition (Female)": "Parkrun Competition",
            "Gary Dean Award": "Gary Dean Award",
            "Under 11 Most Improved T and F": "Juniors",
            "Under 17 Most Improved T and F": "Juniors",
            "Coaches' Award": "Juniors",
            "Junior Winter League Award (Boy)": "Juniors",
            "Junior Winter League Award (Girl)": "Juniors",
        }.get
    )

    df = df.sort_values(["Year", "Type", "Award"])
    df = df[df["Winner"].notna()][["Year", "Type", "Award", "Winner"]]
    return df.columns, tuple(df.itertuples(index=False))


def update_awards(input_path, html_path):
    html_path /= "awards"
    writer = make_page.Writer("PFR Awards", "Awards", html_path)

    filters = tuple(filterable_table.get_filters(LOCAL_PATH / "filters.txt"))
    fields, entries = load_2d(input_path / "Awards" / "2d.tsv")

    writer.write("index", filterable_table.create_page(fields, entries, filters))

    members = read_members.MembersList(input_path / "members.csv").runner_names

    runner_fragments = {
        runner: filterable_table.create_fragment(fields, results)
        for runner in members
        if (results := [x for x in entries if name_in_list(runner, x[3])])
    }

    runner_pages.provide(input_path, "Awards", runner_fragments)
