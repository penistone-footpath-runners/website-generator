import html
import re
from collections.abc import Callable, Container, Hashable, Iterable, Sequence

import competitions.make_page
from competitions import make_page

NUMBER_RE = re.compile(r"\d+\.\d+")


def css(properties: dict[Hashable, object], /) -> str:
    return ";".join([f"{p}:{v}" for p, v in properties.items() if v is not None])


class _Tagger:
    def __getattr__(self, tag: str) -> Callable[..., str]:
        def function(
            *content: object,
            style: str | dict[Hashable, object] | None = None,
            **attributes: str | None,
        ) -> str:
            if style:
                style_string = css(style) if isinstance(style, dict) else style
                if style_string:
                    attributes["style"] = style_string
            attributes_string = "".join(
                [
                    f' {a.removesuffix("_")}="{html.escape(v)}"'
                    for a, v in attributes.items()
                    if v is not None
                ]
            )

            if not content:
                return f"<{tag}{attributes_string}>"
            return f"<{tag}{attributes_string}>{''.join([str(c) for c in content])}</{tag}>"

        setattr(self, tag, function)
        return function


tag = _Tagger()


def create_html_table(
    data: Sequence[Sequence[object]],
    heading_row: int | None = 0,
    column_align: list[str | None] | None = None,
    highlight: Container = (),
    sticky: bool = True,
    col_cols: Sequence[str] | None = None,
    linkables: Iterable[str] = (),
    heading_column: int | None = None,
    col_rows: Sequence[str] | None = None,
    table_css: str | dict[Hashable, object] | None = None,
    big_bold: bool = False,
    round_numbers: bool = False,
):
    """Create an HTML table of some data."""
    if column_align is None:
        column_align = ["left"]
    while len(column_align) < len(data[0]):
        column_align.append(None)

    if len({len(r) for r in data}) != 1:
        raise ValueError("cannot create html from non-rectangular table")

    linkables = {make_page.page_name(p) for p in linkables}
    # Determine how many padding (wierd) spaces are needed to fit all
    # of the numbers
    digitsneeded = [
        max(len(str(round(x))) if isinstance(x, float | int) else 0 for x in column)
        for column in zip(*data)
    ]
    table_code = []
    for i, row in enumerate(data):
        row_code = []
        for j, cell in enumerate(row):
            text = str(cell)
            # Make hover text for floats
            if isinstance(cell, float) and round_numbers:
                # Next line contains unicode funny space
                text = tag.span(
                    str(round(cell)).rjust(digitsneeded[j], " "), title=f"{cell:.5f}"
                )
            elif isinstance(cell, int):
                text = text.rjust(digitsneeded[j], " ")

            # Internal hover text
            elif "." in text and round_numbers and (match := NUMBER_RE.search(text)):
                number = match.group(0)
                text = tag.span(
                    text.replace(number, str(round(float(number)))),
                    title=f"{float(number):.6f}",
                )
            # Make it a link if needed
            if make_page.page_name(text) in linkables:
                text = tag.a(text, href=make_page.page_name(text))

            # Make the largest number bold
            if (
                big_bold
                and isinstance(cell, float)
                and cell == max(x for x in row if isinstance(x, float))
            ):
                text = tag.strong(text)

            # Create if heading
            if i == heading_row or j == heading_column:
                row_code.append(tag.th(text, style={"text-align": column_align[j]}))
                continue

            # Create standard cell
            if (j == 0 and column_align[j] == "left") or (
                j > 0 and column_align[j] == "center"
            ):
                css = {}
            else:
                css = {"text-align": column_align[j]}
            if col_cols:
                css["background-color"] = col_cols[j]
            elif col_rows:
                css["background-color"] = col_rows[i]
            row_code.append(tag.td(text, style=css))

        row_classes = []
        if row in highlight:
            row_classes.append("highlighted")
        if sticky and i == heading_row:
            row_classes.append("sticky")
        table_code.append(
            tag.tr(
                "".join(row_code),
                class_=(" ".join(row_classes) if row_classes else None),
            )
        )

    return tag.table("\n".join(table_code), style=table_css)


def runner_pages_button(runner: str) -> str:
    first_name = runner.split(" ")[0]
    return tag.div(
        tag.a(
            f"Click to see results for all of {first_name}’s competitions.",
            href=f"../runners/{competitions.make_page.page_name(runner)}",
        ),
        class_="buttons",
    )
