import re
import string
from dataclasses import dataclass
from functools import cache
from pathlib import Path

LOCAL_PATH = Path(__file__).resolve().parent

HEADER = string.Template(
    "".join(
        [
            f"{r.strip()}\n"
            for r in (LOCAL_PATH / "header.html").read_text().splitlines()
        ]
    )
)

CHARACTER_MAP = str.maketrans(
    {c: c.casefold() for c in string.ascii_letters + string.digits + ".-"}
    | {c: "-" for c in " _\N{EN DASH}"}
    | {c: "" for c in ",;:()[]{}"}
)

SHRINK_DASH_SEQUENCES = re.compile(r"-+")


@cache
def page_name(name: str, /) -> str:
    return SHRINK_DASH_SEQUENCES.sub("-", name.translate(CHARACTER_MAP))


@dataclass
class Writer:
    title: str
    heading: str
    output_path: Path
    head: str = ""

    def __post_init__(self):
        self.output_path.mkdir(exist_ok=True)

    def write(
        self, filename, content, title=None, heading=None, head=None, extension=".html"
    ):
        if title is None:
            title = self.title
        if heading is None:
            heading = self.heading
        if head is None:
            head = self.head
        (self.output_path / (filename + extension)).write_text(
            HEADER.substitute(**locals())
        )
