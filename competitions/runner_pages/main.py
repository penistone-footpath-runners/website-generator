from dataclasses import dataclass

from competitions import make_page, read_members
from competitions.html_gen import tag

from . import read


@dataclass(frozen=True)
class Competition:
    name: str
    directory: str
    default_page: str
    default_text: str
    individual_pages: bool = True


def get_runners(path) -> frozenset:
    return read_members.MembersList(path / "members.csv").runner_names


def update_runner_pages(input_path, path_to_html):
    path_to_html /= "runners"
    writer = make_page.Writer("PFR List of Runners", "List of Runners", path_to_html)

    runners = read(input_path)
    valid_runners = get_runners(input_path)
    runners = {r: v for r, v in runners.items() if r in valid_runners}

    years = dict(
        [x.strip() for x in r.split("=")]
        for r in (input_path / "runner_page_years.txt").read_text().splitlines()
        if not r.isspace()
    )
    competitions = (
        Competition("Road Standards", "road-standards", "", "No road standards."),
        Competition(
            "Club Records", "records", "", "No club records.", individual_pages=False
        ),
        Competition("Awards", "awards", "", "No awards.", individual_pages=False),
        Competition(
            "Club Championship",
            f"championship-{years['championship']}",
            "challenge",
            "No Club Championship races.",
        ),
        Competition(
            "Fell League",
            f"fell-league-{years['fell league']}",
            "races",
            "No qualifying fell races.",
        ),
        Competition(
            "Parkrun", f"parkrun-{years['parkrun']}", "all-parkruns", "No Parkruns."
        ),
    )
    # Make per-runner pages
    for runner in runners:
        page = []
        for competition in competitions:
            comp_url = f"../{competition.directory}"
            # Check if the runner does that competition
            if competition.name not in runners[runner]:
                page.append(
                    tag.h3(
                        tag.a(
                            competition.name,
                            class_="headinglink",
                            href=f"{comp_url}/{competition.default_page}",
                        )
                    )
                    + tag.em(competition.default_text)
                    + "<br><br>"
                )
                continue
            if competition.individual_pages:
                comp_url += f"/{make_page.page_name(runner)}"
            page.append(
                tag.h3(tag.a(competition.name, class_="headinglink", href=comp_url))
            )
            page.append(runners[runner][competition.name])
            page.append("<br><br>")
        writer.write(
            make_page.page_name(runner),
            "".join(page),
            title=f"PFR {runner}",
            heading=runner,
        )

    # Make list of runners
    main_table = tag.ul(
        "".join(
            tag.li(tag.a(runner, href=make_page.page_name(runner)))
            for runner in sorted(runners, key=lambda x: x.split(" ")[1::-1])
        )
    )
    writer.write("index", main_table)
