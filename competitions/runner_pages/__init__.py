import json
import lzma
from pathlib import Path

PAGES_FILE = "pages.json.xz"

PRESET = 9


def read(input_path: Path) -> dict[str, dict[str, str]]:
    try:
        with lzma.open(input_path / PAGES_FILE, "rt") as f:
            return json.load(f)
    except FileNotFoundError:
        runners: dict[str, dict[str, str]] = {}
        with lzma.open(input_path / PAGES_FILE, "wt", preset=PRESET) as f:
            json.dump(runners, f)
        return runners


def provide(input_path: Path, competition: str, fragments: dict[str, str]) -> None:
    runner_pages = read(input_path)
    for runner in fragments:
        runner_pages.setdefault(runner, {})

    for name, runner_results in runner_pages.items():
        if name in fragments:
            runner_results[competition] = fragments[name]
        elif competition in runner_results:
            del runner_results[competition]

    with lzma.open(input_path / PAGES_FILE, "wt", preset=PRESET) as f:
        json.dump(runner_pages, f, separators=(",", ":"))
