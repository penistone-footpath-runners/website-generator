import math

from competitions.times import Time

from .constants import WORST_HANDICAP


class Runner:
    def __init__(self, name, gender, date_of_birth, start_time):
        self.name = name
        self.gender = gender
        self.DOB = date_of_birth
        self.PB = WORST_HANDICAP - Time(start_time)
        self.round_pb()

        self.races = []
        self.set_start_time(False)

    def add_race(self, race):
        """Add a new race to the runner."""
        # Make the time the actual time
        start_time = self.set_start_time(race["Start Time"])
        race["Finish Time"] = race["Actual Time"]
        race["Actual Time"] = race["Actual Time"] - start_time

        # Add the PB column
        race["Previous PB"] = str(self.PB)

        # Determine if the runner has a PB
        race["PB?"] = ""
        if self.PB is None or race["Actual Time"] < self.PB:
            self.PB = race["Actual Time"]
            self.round_pb()

        self.races.append(race)

    def get_average(self, count=None):
        """Find the runner's average time this year."""
        if self.races:
            counters = self.races if count is None else self.races[-count:]

            return Time(
                sum(race["Actual Time"].time for race in counters) / len(counters)
            ).round()
        return None

    def get_best(self):
        """Find the best time the runner has achieved this year."""
        if self.races:
            return Time(min(race["Actual Time"].time for race in self.races))
        return None

    def get_best_index(self):
        """Find the best time the runner has achieved this year."""
        best = self.get_best()
        for race in self.races:
            if race["Actual Time"] == best:
                return race["Number"]
        raise AssertionError("best time not in any race")

    def get_points(self):
        """Determine the total points of a runner."""
        return sum(race["Points"] for race in self.races)

    def round_pb(self, val=None):
        """Round the PB to the nearest five seconds."""
        if val is None:
            self.effective_PB = Time(math.ceil(self.PB.time / 10) * 10)
            return None
        return Time(math.ceil(float(val.time / 10)) * 10)

    def set_start_time(self, start_time):
        """Calculate the race's start time."""
        if not start_time:
            start_time = WORST_HANDICAP - self.effective_PB
        self.start_time = start_time
        return start_time

    def age_at(self, date):
        """Return the runner's age on a particular date."""
        approx = int(date.split("-")[0]) - int(self.DOB.split("-")[0])
        # Determine if their birthday has been reached this year
        if date[5:] < self.DOB[5:]:
            approx -= 1
        return approx
