import csv
from pathlib import Path

from competitions import make_page, read_members
from competitions.constants import SEXES
from competitions.times import Time

from . import html
from .constants import PAGES, RACE_COUNT, WORST_HANDICAP
from .runner import Runner

LOCAL_PATH = Path(__file__).resolve().parent


MAIN_HEADS = ["Position", "Name", "Races Run", "Points"]
FAST_HEADS = ["Position", "Name", "Time", "Race"]
LEAGUE_ALIGNS = ["center", "left"]

RACE_ALIGNS = ["center", "left"]


class RunnerNotFoundError(Exception):
    pass


def csv_to_dict_table(csv_file, headings):
    """Convert a CSV string to a list of dicts."""
    return [dict(zip(headings, row)) for row in csv.reader(csv_file)]


def dict_table_to_table(table, headings):
    return [headings] + [[row[heading] for heading in headings] for row in table]


with open(LOCAL_PATH / "age_grades.csv") as f:
    AGE_GRADES = list(csv.reader(f))


def adjust_time(time, sex, age):
    """Adjust the time according to WMA table."""
    if not 5 <= age <= 100:
        raise ValueError(f"age ({age}) not in [5, 100]")
    if sex.casefold() in ("m", "male", "boy"):
        factor = float(AGE_GRADES[age][0])
    elif sex.casefold() in ("f", "female", "girl"):
        factor = float(AGE_GRADES[age][1])
    else:
        raise ValueError(f"Invalid sex: {sex}")

    return time * factor


def update_handicap(input_path, path_to_html, year):
    path_to_html /= f"senior-winter-handicap-{year}"
    writer = make_page.Writer(
        "PFR Winter Handicap", f"{year} Winter Handicap", path_to_html
    )
    handicap_inputs = input_path / "SeniorWinterHandicap" / str(year)

    dates = [None] + (handicap_inputs / "dates.txt").read_text().splitlines()

    # Load the runner data
    with open(handicap_inputs / "SeniorRunners.csv") as f:
        runner_data = list(csv.reader(f))

    # Load members data
    def get_members_from(path: Path) -> dict[str, dict[str, str]]:
        return {
            r.system_name: {"sex": r.sex[0], "date of birth": r.date_of_birth}
            for r in read_members.MembersList(path).get_runners(
                ("System name", "Sex", "Date of birth")
            )
        }

    members = get_members_from(input_path / "members.csv") | get_members_from(
        handicap_inputs / "NonMembers.csv"
    )

    runners = {}

    for row in runner_data:
        # Convert the rows to Runner objects
        name, handicap = row
        try:
            member = members[name]
        except KeyError:
            raise RunnerNotFoundError(
                f"{name} is missing from the members file"
            ) from None
        runners[name] = Runner(name, member["sex"], member["date of birth"], handicap)

    race_files = []

    for race in range(1, RACE_COUNT + 1):
        try:
            with open(handicap_inputs / f"{race}.csv") as f:
                race_files.append(
                    (
                        csv_to_dict_table(
                            f, ("Rank", "Name", "Actual Time", "Start Time")
                        ),
                        race,
                    )
                )
        except FileNotFoundError:
            continue

    valid_races = [x[1] for x in race_files]

    for race_file in race_files:
        race = race_file[1]
        r = f"Race {race}"
        race_data = race_file[0]
        for row in race_data:
            # Find the runner
            row["Number"] = race
            try:
                runner = runners[row["Name"]]
            except KeyError as e:
                raise RunnerNotFoundError(
                    f"{row['Name']} is not in the handicaps file"
                ) from e
            if row["Rank"] == "":
                runner.start_time = Time(row["Start Time"])
                continue
            # Make times proper times
            row["Actual Time"] = Time(row["Actual Time"])
            # Add age category column
            row["Gender"] = runner.gender
            # Add the race to the corresponding runner
            runner.add_race(row)
            # Use this to get average and best times
            row["Series Best"] = runner.get_best()
            row["Average"] = runner.get_average()
            row["Estimated Time"] = WORST_HANDICAP - runner.start_time
            row["+/−"] = format(Time(row["Actual Time"] - row["Estimated Time"]), "+")
            row["Adjusted Time"] = adjust_time(
                row["Actual Time"], runner.gender, runner.age_at(dates[race])
            )

        race_data = [x for x in race_data if x["Rank"] != ""]
        # Add the rows relevant to adjusted times
        new_race_data = sorted(race_data, key=lambda x: x["Adjusted Time"])
        for position, row in enumerate(new_race_data, start=1):
            points = max(26 - position, 5)
            row["Points"] = points
            runners[row["Name"]].races[-1]["Points"] = points

        for page in PAGES:
            cat_data = sorted(
                filter(page.filter_, race_data), key=lambda x: x[page.sort_column]
            )
            for pos, row in enumerate(cat_data, start=1):
                row["Position"] = pos
            race_name = f"{r} – {page.name}"
            table = dict_table_to_table(cat_data, page.columns)
            race_file = html.create_html_page(
                race_name, table, page.name, r, valid_races, column_align=RACE_ALIGNS
            )
            writer.write(f"{make_page.page_name(race_name)}", race_file)

        # Make the next-race handicaps
        start_times = []
        for runner in runners.values():
            if runner.races:
                last_time = Time(runner.races[-1]["Actual Time"])
                predicted = Time(
                    runner.round_pb(
                        min(Time(WORST_HANDICAP - runner.start_time), last_time)
                    )
                )
            else:
                last_time = ""
                predicted = Time(WORST_HANDICAP - runner.start_time)
            row = [
                runner.name,
                " ".join(runner.name.split(" ")[1:]),
                Time(WORST_HANDICAP - runner.start_time),
                last_time,
                predicted,
            ]
            t = runner.get_average(3)
            if t is not None:
                row.append(Time(t))
            else:
                row.append(None)
            t = runner.get_best()
            if t is not None:
                row.append(Time(t))
            else:
                row.append(None)

            start_times.append(row)

        start_times.sort(key=lambda x: x[0])
        start_times.sort(key=lambda x: x[1])

        start_times = [
            [format(t, "00") if isinstance(t, Time) else t for t in row]
            for row in start_times
        ]

        times_page = html.create_html_page(
            f"{r} – Start Times",
            [
                [
                    "Name",
                    "Surname",
                    "Start Time for This Race",
                    "Time",
                    "Next Predicted Time",
                    "Average Time",
                    "Best Time",
                ]
            ]
            + start_times,
            "Start Times",
            "Start Times",
            [],
        )
        writer.write("start-times", times_page)

    # Create league tables
    table = [
        ["", runner.name, len(runner.races), runner.get_points(), runner.gender]
        for runner in runners.values()
    ]

    # Filter out those with no races
    table = [x for x in table if x[2] > 0]

    race_name = "League Table"
    # Not actual filtered
    filtered = table.copy()
    # Sort by PB, races, then points
    filtered.sort(key=lambda x: x[2])
    filtered.sort(key=lambda x: x[3], reverse=True)
    for runner in range(len(filtered)):
        # Add the position
        if (
            filtered[runner][3] == filtered[runner - 1][3]
            and filtered[runner][2] == filtered[runner - 1][2]
        ):
            filtered[runner][0] = "="
        else:
            filtered[runner][0] = str(runner + 1)
        # Remove the gender and category fields, as they shouldn't
        # be in the final table: they were just needed for filtering.
        filtered[runner] = filtered[runner][:-1]

    race_file = html.create_html_page(
        "WMA League Table",
        [MAIN_HEADS] + filtered,
        "",
        race_name,
        valid_races,
        column_align=LEAGUE_ALIGNS,
    )
    writer.write(f"{make_page.page_name(race_name)}", race_file)

    # Make the best times pages
    table = sorted((x for x in runners.values() if x.races), key=lambda x: x.get_best())

    for gender in SEXES.values():
        race_name = f"Fastest {gender}"
        filtered = [x for x in table if x.gender == gender[0]]
        new_table = [
            [pos, runner.name, runner.get_best(), runner.get_best_index()]
            for pos, runner in enumerate(filtered, start=1)
        ]

        race_file = html.create_html_page(
            f"Fastest Times {gender}",
            [FAST_HEADS] + new_table,
            "",
            race_name,
            valid_races,
            column_align=LEAGUE_ALIGNS,
        )
        writer.write(f"{make_page.page_name(race_name)}", race_file)
