"""Stores various constants needed by some files."""

from collections.abc import Callable
from dataclasses import dataclass

from competitions import times


@dataclass(frozen=True)
class Page:
    name: str
    sort_column: str
    columns: tuple[str, ...]
    filter_: Callable[[dict], bool]


WORST_HANDICAP = times.Time("52:00")
PAGES = (
    Page(
        "Race Result",
        "Finish Time",
        ("Position", "Name", "Estimated Time", "Finish Time", "Actual Time", "+/−"),
        lambda _: True,
    ),
    Page(
        "Fastest Times Female",
        "Actual Time",
        ("Position", "Name", "Actual Time"),
        lambda x: x["Gender"] == "F",
    ),
    Page(
        "Fastest Times Male",
        "Actual Time",
        ("Position", "Name", "Actual Time"),
        lambda x: x["Gender"] == "M",
    ),
    Page(
        "WMA Adjusted Times",
        "Adjusted Time",
        ("Position", "Name", "Actual Time", "Adjusted Time", "Points"),
        lambda _: True,
    ),
)

RACE_COUNT = 6
