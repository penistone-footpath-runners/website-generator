from competitions import make_page
from competitions.html_gen import create_html_table, tag

from .constants import PAGES, RACE_COUNT

HEAD = (
    tag.div(
        tag.a(
            "Race and WMA Information",
            href="https://pfrac.co.uk/competitions/senior-winter-handicap",
        ),
        class_="buttons",
    )
    + tag.br()
    + tag.br()
)


def cell_button(text, link, selected):
    return tag.th(tag.a(text, href=link), class_=("selected" if selected else None))


def create_html_page(page_name, data, sort_method, r, valid_races, column_align=None):
    """Create an HTML page for some data."""
    non_races = ["League Table", "Fastest Female", "Fastest Male"]
    races = [(x, f"Race {x}") for x in range(1, RACE_COUNT + 1)]
    top_table = tag.table(
        tag.tr(
            "".join(
                cell_button(page, f"{make_page.page_name(page)}", page == r)
                for page in non_races
            )
            + "".join(
                (
                    cell_button(
                        page,
                        make_page.page_name(
                            f"{page}-{'race-result' if r in non_races else sort_method}"
                        ),
                        page == r,
                    )
                    if n in valid_races
                    else tag.th(page)
                )
                for n, page in races
            )
        ),
        style={"border-collapse": "separate", "table-layout": "fixed"},
    )

    # Create the sort order table
    if r not in non_races:
        top_table += "<br><br>" + tag.table(
            tag.tr(
                "".join(
                    tag.th(
                        tag.a(page.name, href=make_page.page_name(f"{r}-{page.name}")),
                        class_=("selected" if page.name == sort_method else None),
                    )
                    for page in PAGES
                )
            ),
            style={
                "border-collapse": "separate",
                "width": "50rem",
                "table-layout": "fixed",
                "margin-left": "auto",
                "margin-right": "auto",
            },
        )

    return (
        HEAD
        + top_table
        + tag.h2(page_name)
        + create_html_table(data, heading_row=0, column_align=column_align)
    )
